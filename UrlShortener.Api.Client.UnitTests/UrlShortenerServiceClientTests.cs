﻿using Feratel.Deskline.Infrastructure.HttpClient.Interfaces;
using Feratel.Deskline.Infrastructure.HttpClient.Services;
using Feratel.Deskline.Infrastructure.Json.Interfaces;
using UrlShortener.Api.Client;
using UrlShortener.Api.Client.Interfaces;
using UrlShortener.Api.Client.Services;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Deskline.UrlShortener.Api.Client.UnitTests
{
    [TestClass]
    public class UrlShortenerServiceClientTests
    {
        public class TestClientService : UrlShortenerService
        {
            public TestClientService(IHttpClient<UrlShortenerService> httpClient, IUrlShortenerConfiguration config) : base(httpClient, config)
            {
                
            }

            public new string GetBaseUrl()
            {
                return base.GetBaseUrl();
            }
        }


        [TestMethod]
        public void GetBaseUrl_Should_ReturnUrl()
        {
            #region Arrange
            string url = "http://localhosttest:5000";
            var client = new Mock<System.Net.Http.HttpClient>();
            var logger = new Mock<ILogger<HttpClient<UrlShortenerService>>>();
            var jsonConverter = new Mock<IJsonConverter>();
            var httpClient = new HttpClient<UrlShortenerService>(client.Object, logger.Object, jsonConverter.Object);

            var config = new UrlShortenerConfiguration(url);
            #endregion

            #region Act
            var sut = new TestClientService(httpClient, config);

            string result = sut.GetBaseUrl();
            #endregion

            #region Assert
            Assert.AreEqual(result, url);
            #endregion


        }

        //[TestMethod]
        //public void GetBaseUrl_ShouldReturnUrl ()
        //{
        //    #region Arrange
        //    string requestUrl = string.Empty;
        //    string url = "http://localhosttest:5000";
        //    var client = new Mock<System.Net.Http.HttpClient>();
        //    var logger = new Mock<ILogger<HttpClient<UrlShortenerService>>>();
        //    var jsonConverter = new Mock<IJsonConverter>();
        //
        //    client.Setup(c => c.SendAsync(It.IsAny<HttpRequestMessage>())).Callback<HttpRequestMessage>((req) =>
        //    {
        //        requestUrl = req.RequestUri.ToString();
        //    });
        //
        //    var config = new UrlShortenerConfiguration(url);
        //    var httpClient = new HttpClient<UrlShortenerService>(client.Object, logger.Object, jsonConverter.Object);
        //    #endregion
        //
        //    #region Act
        //    var sut = new UrlShortenerService(httpClient, config);
        //
        //    var result =  sut.GetUrlShortenerHealthCheckStatus();
        //    #endregion
        //
        //    #region Assert
        //    Assert.AreEqual(result, requestUrl);
        //    #endregion
        //
        //}
    }
}
