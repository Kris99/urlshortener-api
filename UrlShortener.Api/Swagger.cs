﻿using Lamar;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using UrlShortener.BusinessLogic.General;

namespace UrlShortener.Api
{
    public static class SwaggerExtensions
    {
        public static void AddSwagger(this ServiceRegistry service, ApplicationConfig config)
        {
            service.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("Deskline.UrlShortener.Api", new OpenApiInfo
                {
                    Title = "Deskline.UrlShortener.Api",
                    Version = "v1"
                });
                //c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                //{
                //    Type = SecuritySchemeType.OAuth2,
                //    Flows = new OpenApiOAuthFlows
                //    {
                //        Implicit = new OpenApiOAuthFlow
                //        {
                //            AuthorizationUrl = new Uri(config.GetAuthorityLocation() + "/connect/authorize"),
                //            TokenUrl = new Uri(config.GetAuthorityLocation() + "/connect/token"),
                //            Scopes = new Dictionary<string, string>() { { Constants.DIS_API_SCOPE_ID, "Deskline.UrlShortener.API" } },
                //        }
                //    },
                //    In = ParameterLocation.Header,
                //    Scheme = JwtBearerDefaults.AuthenticationScheme
                //});
                //c.AddSecurityRequirement(new OpenApiSecurityRequirement
                //{
                //    {
                //        new OpenApiSecurityScheme
                //        {
                //            Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "oauth2" }
                //        },
                //        new List<string>()
                //    }
                //});
            });
        }       
    }
}
