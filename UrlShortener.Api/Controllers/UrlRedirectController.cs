﻿using UrlShortener.BusinessLogic.DbLayer.DTO;
using Feratel.Deskline.Infrastructure.ErrorHandling.Core;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Feratel.Base.Web.Log;
using Microsoft.Extensions.Primitives;
using UrlShortener.Api.ViewModels;
using UrlShortener.BusinessLogic.Services.Interfaces;
using UrlShortener.BusinessLogic.Services;
using UrlShortener.Contracts.Queries;

namespace UrlShortener.API.Controllers
{
    [Route("")]
    public class UrlRedirectController : Controller
    {
        private readonly ILogger logger;
        private readonly IUrlRedirectService urlRedirectService;
        private readonly IUrlStatisticsService urlStatService;

        public UrlRedirectController(ILogger<UrlRedirectController> logger, IUrlRedirectService urlRedirectService, IUrlStatisticsService urlStatService)
        {
            this.logger = logger;
            this.urlRedirectService = urlRedirectService;
            this.urlStatService = urlStatService;
        }

        [HttpGet("/{code}")]
        public IActionResult UrlRedirect([FromRoute] string code)
        {
            try       
            {
                UrlDetails url = this.urlRedirectService.GetUrlByCodeForRedirection(code);

                if (url.HasPassword)
                {
                    this.logger.LogDebug("Redirected to password confirmation page.");

                    this.SetPageLanguage();

                    var passwordModel = new PasswordViewModel()
                    {
                        Code = code
                    };

                    return View("Auth", passwordModel);
                }

                else
                {
                    this.urlStatService.InsertStats(Guid.Parse(url.Id));

                    this.logger.LogDebug("Redirected to: {url}", url.Url);

                    if (url.Method == "GET")
                    {
                        return Redirect(url.Url);
                    }
                    else if(url.Method == "POST")
                    {
                        RedirectViewModel redirectModel = new RedirectViewModel()
                        {
                            Url = url.Url,
                            Method = url.Method,
                            Parameters = url.Options.Parameters
                        };

                        return View("Redirect", redirectModel);
                    }

                    return NoContent();
                }
            }
            catch (ApiException ex)
            {
                this.logger.LogDebug(ex.Message);

                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                this.logger.LogException(ex);

                return StatusCode(500);
            }
        }

        [HttpPost("/{code}")]
        public IActionResult AuthorizedRedirection([FromRoute] string code, string inputPassword)
        {
            try
            {
                AuthorizedUrl url = this.urlRedirectService.GetAuthorizedUrlByCode(code, inputPassword);

                if (!url.Authorized)
                {
                    this.logger.LogDebug("Wrong Password.");

                    var passwordModel = new PasswordViewModel()
                    {
                        Code = url.Code,
                        ErrorMessage = "Wrong password"
                    };

                    this.SetPageLanguage();

                    return View("Auth", passwordModel);
                }

                this.logger.LogDebug("Password confirmation was successful. Redirected to: {url}", url.Url);

                this.urlStatService.InsertStats(Guid.Parse(url.Id));

                if (url.Method == "GET")
                {
                    return Redirect(url.Url);
                }
                else if(url.Method == "POST")
                {
                    RedirectViewModel redirectModel = new RedirectViewModel()
                    {
                        Url = url.Url,
                        Method = url.Method,
                        Parameters = url.Options.Parameters
                    };

                    return View("Redirect", redirectModel);
                }

                return NoContent();
            }
            catch (ApiException ex)
            {
                this.logger.LogDebug(ex.Message);

                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                this.logger.LogException(ex);

                return StatusCode(500);
            }
        }

        private void SetPageLanguage()
        {
            var accLang = new StringValues();

            this.Request.Headers.TryGetValue("Accept-Language", out accLang);
            string lang = null;

            if (accLang.Count > 0 && !string.IsNullOrEmpty(accLang[0]))
            {
                lang = accLang[0].Substring(0, 2);
            }

            TranslateService.SetLanguage(lang);
        }

    }
}
