﻿using Feratel.Deskline.Infrastructure.ErrorHandling.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using Feratel.Base.Web.Log;
using UrlShortener.BusinessLogic.General;
using UrlShortener.BusinessLogic.Services.Interfaces;
using UrlShortener.Contracts.Commands;
using UrlShortener.Contracts.Queries;

namespace UrlShortener.Api.Controllers
{
    [ApiController]
    [Route("/urls")]
    public class UrlShortenerController : Controller
    {
        private readonly ILogger logger;
        private readonly IUrlShortenerService urlShortenerService;

        public UrlShortenerController(ILogger<UrlShortenerController> logger, IUrlShortenerService urlShortenerService)
        {
            this.logger = logger;
            this.urlShortenerService = urlShortenerService;
        }

        [HttpPost]
        //[Authorize(Constants.DIS_API_SECURITY_POLICY_EDIT)]
        public async Task<IActionResult> CreateUrl([FromBody] CreateUrl model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                Guid id = await urlShortenerService.CreateUrl(model);

                logger.LogDebug("Url with Id:[{id}] was inserted", id);

                UrlDetails response = urlShortenerService.GetUrlById(id);

                logger.LogDebug("Url with ID:[{id}] was received.", id);

                return Ok(response);
            }
            catch (ApiException ex)
            {
                logger.LogDebug(ex.Message);

                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                logger.LogException(ex);

                return StatusCode(500);
            }

        }

        [HttpGet("list")]
        //[Authorize(Constants.DIS_API_SECURITY_POLICY_VIEW)]
        public IActionResult GetAllUrls()
        {
            try
            {
                UrlCollection urlCollection = urlShortenerService.GetAllUrls();

                logger.LogDebug("All available urls were retrieved.");

                return Ok(urlCollection);
            }
            catch (ApiException ex)
            {
                logger.LogDebug(ex.Message);

                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                logger.LogException(ex);

                return StatusCode(500);
            }
        }

        [HttpGet("{id}")]
        //[Authorize(Constants.DIS_API_SECURITY_POLICY_VIEW)]
        public IActionResult GetUrlById(Guid id)
        {
            try
            {
                UrlDetails response = urlShortenerService.GetUrlById(id);

                logger.LogDebug("Url with ID:[{id}] was received.", id);

                return Ok(response);
            }
            catch (ApiException ex)
            {
                logger.LogDebug(ex.Message);

                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                logger.LogException(ex);

                return StatusCode(500);
            }

        }

        [HttpPut("{id}")]
        //[Authorize(Constants.DIS_API_SECURITY_POLICY_EDIT)]
        public IActionResult UpdateUrlById(Guid id, [FromBody] UpdateUrl model)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                urlShortenerService.UpdateById(id, model);

                logger.LogDebug("Url with ID:[{id}] was received.", id);

                UrlDetails response = urlShortenerService.GetUrlById(id);

                logger.LogDebug("Url with ID:[{id}] was received.", id);

                return Ok(response);
            }
            catch (ApiException ex)
            {
                logger.LogDebug(ex.Message);

                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                logger.LogException(ex);

                return StatusCode(500);
            }

        }

        [HttpDelete("{id}")]
        //[Authorize(Constants.DIS_API_SECURITY_POLICY_EDIT)]
        public IActionResult DeleteUrlById(Guid id)
        {
            try
            {
                urlShortenerService.DeleteById(id);

                logger.LogDebug("Url with id: [{id}] was deleted", id);
            }
            catch (ApiException ex)
            {
                logger.LogDebug(ex.Message);

                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                logger.LogException(ex);

                return StatusCode(500);
            }

            return Ok($"Url with id: [{id}] was deleted");

        }

    }
}
