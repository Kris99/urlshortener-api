﻿using Feratel.Deskline.Infrastructure.ErrorHandling.Core;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using Feratel.Base.Web.Log;
using UrlShortener.BusinessLogic.Services.Interfaces;
using UrlShortener.Contracts.Queries;

namespace UrlShortener.Api.Controllers
{
    [Route("stats")]
    //[Authorize(Constants.DIS_API_SECURITY_POLICY_VIEW)]
    public class UrlStatsController : Controller
    {
        private readonly ILogger<UrlStatsController> logger;
        private readonly IUrlStatisticsService statisticsService;

        public UrlStatsController(ILogger<UrlStatsController> logger, IUrlStatisticsService statisticsService)
        {
            this.logger = logger;
            this.statisticsService = statisticsService;
        }

        [HttpGet("{id}")]
        public IActionResult GetUrlClicks(Guid id)
        {
            try
            {
                Stats stats = statisticsService.GetClicks(id);

                logger.LogDebug("Click count for url with id: {id} was received.", id);

                return Ok(stats);
            }
            catch (ApiException ex)
            {
                logger.LogDebug(ex.Message);

                return NotFound();
            }
            catch (Exception ex)
            {
                logger.LogException(ex);

                return StatusCode(500);
            }

        }
    }
}
