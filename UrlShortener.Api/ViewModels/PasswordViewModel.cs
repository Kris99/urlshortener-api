﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UrlShortener.Api.ViewModels
{
    public class PasswordViewModel
    {
        [Required]
        public string Code { get; set; }

        [Required]
        [StringLength(28, MinimumLength = 4, ErrorMessage = "The password must be at least 4 characters long.")]
        public string Password { get; set; }

        public string ErrorMessage { get; set; }
    }
}
