﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UrlShortener.Api.ViewModels
{
    public class RedirectViewModel
    {
        public string Url { get; set; }

        public string Code { get; set; }

        public string Method { get; set; }

        public Dictionary<string, object> Parameters { get; set; }

        public string ErrorMessage { get; set; }
    }
}
