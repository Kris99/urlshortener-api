﻿using Feratel.Deskline.Infrastructure.HealthCheck.Models.Extensions;
using Feratel.Deskline.Infrastructure.HealthCheck.Extensions;
using Feratel.Deskline.Infrastructure.ErrorHandling.Lamar;
using Feratel.Deskline.Infrastructure.DesklineLog.Lamar;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Lamar;
using Feratel.Deskline.Infrastructure.AppMetrics;
using Feratel.Deskline.Base;
using UrlShortener.BusinessLogic;
using UrlShortener.BusinessLogic.General;

namespace UrlShortener.Api
{
    public class Startup
    {
        private IConfiguration Configuration { get; set; }

        public Startup(IConfiguration config)
        {
            this.Configuration = config;
        }

        public void ConfigureContainer (ServiceRegistry services)
        {
            var config = new ApplicationConfig(Configuration);

            services.AddApiSecurity(config);
            services.AddSwagger(config);
            services.AddCors();
            services.AddHttpContextAccessor();
            services.AddControllersWithViews();
            services.AddRazorPages().AddRazorRuntimeCompilation();

            #region DeskLine packages
            services.AddAppMetrics(config);
            services.AddDesklineHealthChecks(config);
            services.AddDesklineLog<ApplicationConfig>((short)LogSource.URLShortener, "UrlShortener.API");
            #endregion

            #region UrlServices
            services.AddSingleton(config);
            services.IncludeRegistry(new UrlServiceRegistry());
            #endregion

        }

        public void Configure(IApplicationBuilder app,
                              IWebHostEnvironment env,
                              ILoggerFactory loggerFactory)
        {
            app.UseStaticFiles();

            #region Error handling
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseErrorHandling();
            #endregion

            #region Logging
            loggerFactory.AddDesklineLog();
            #endregion

            #region CORS
            app.UseCors(builder => builder
                                    .AllowAnyHeader()
                                    .AllowAnyMethod()
                                    .AllowAnyOrigin());
            #endregion

            #region Swagger
            if (env.IsDevelopment() || env.IsStaging() || env.EnvironmentName.ToLower() == "sofia")
            {
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/Deskline.UrlShortener.Api/swagger.json", "Deskline.UrlShortener.Api");
                    c.OAuthClientId("Deskline.UrlShortener.API.Swagger");
                });
            }
            #endregion

            #region Routing
            app.UseRouting();
            #endregion

            #region Security
            app.UseAuthentication();
            app.UseAuthorization();
            #endregion

            #region Endpoints
            app.UseEndpoints(endpoint => 
            {
                endpoint.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action}/{id?}"
                    );

                endpoint.MapDesklineHealthChecks(new DesklineHealthCheckApplicationSettings()
                {
                    HealthEndpoint = new DesklineHealthCheckEndpoint()
                    {
                        Endpoint = "health/status"
                    }
                
                }); 
            });
            #endregion
        }
    }
}
