﻿
function buttonEnabling() {

    var inputPassword = document.getElementById("inputPassword").value;
    var button = document.getElementById("btn");

    if (inputPassword.length >= 4) {
        button.disabled = false;
    }
    else {
        button.disabled = true;
    }
}
