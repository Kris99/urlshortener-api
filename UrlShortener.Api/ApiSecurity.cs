﻿using IdentityServer4.AccessTokenValidation;
using Lamar;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using UrlShortener.BusinessLogic.General;
using UrlShortener.BusinessLogic.General.Interfaces;

namespace UrlShortener.Api
{
    public static class ApiSecurityExtensions
    {
        public static void AddApiSecurity(this ServiceRegistry services, IAuthorizationSettings settings)
        {
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                    .AddIdentityServerAuthentication(options =>
                    {
                        options.Authority = settings.GetAuthorityLocation();
                        options.ApiName = Constants.DIS_API_SCOPE_ID;
                        options.RequireHttpsMetadata = false;
                    });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(Constants.DIS_API_SECURITY_POLICY_EDIT, builder =>
                {
                    builder.AuthenticationSchemes.Add(IdentityServerAuthenticationDefaults.AuthenticationScheme);
                    builder.RequireScope(Constants.DIS_API_SCOPE_ID);
                    builder.RequireClaim(Constants.DIS_API_CLAIM_USAPI_ROLE, Constants.DIS_API_CONFIGURATION_ROLE_ADMIN);
                });
                options.AddPolicy(Constants.DIS_API_SECURITY_POLICY_VIEW, builder =>
                {
                    builder.AuthenticationSchemes.Add(IdentityServerAuthenticationDefaults.AuthenticationScheme);
                    builder.RequireScope(Constants.DIS_API_SCOPE_ID);
                    builder.RequireClaim(Constants.DIS_API_CLAIM_USAPI_ROLE, Constants.DIS_API_CONFIGURATION_ROLE_ADMIN, Constants.DIS_API_CONFIGURATION_ROLE_NORMAL);
                });
            });
        }
    }
}

                    