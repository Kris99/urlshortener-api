﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace UrlShortener.Contracts.Commands.UrlObjects
{
    public class UrlOptions
    {
        [JsonProperty("parameters")]
        public Dictionary<string, object> Parameters { get; set; } = new Dictionary<string, object>();
    }
}
