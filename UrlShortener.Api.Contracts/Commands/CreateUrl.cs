﻿using System.ComponentModel.DataAnnotations;
using System;
using UrlShortener.Contracts.CustomAttributes;
using Newtonsoft.Json;
using UrlShortener.Contracts.Commands.UrlObjects;

namespace UrlShortener.Contracts.Commands
{
    public class CreateUrl
    {
        [Url]
        [Required]
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("code")]
        [MaxLength(50, ErrorMessage = "Please choose a shorter path. Maximum allowed symbols are 12.")]
        public string Code { get; set; }

        [JsonProperty("validUntil")]
        public DateTime? ValidUntil { get; set; }

        [JsonProperty("validDays")]
        [Range(0, 1500, ErrorMessage = "The given days must be a positive number.")]
        public int ValidDays { get; set; }

        [JsonProperty("password")]
        [StringLength(28, MinimumLength = 4, ErrorMessage = "The password must be long between 4 and 10 characters")]
        public string Password { get; set; }

        [UrlMethod]
        [JsonProperty("method")]
        public string Method { get; set; }

        [JsonProperty("options")]
        public UrlOptions Options { get; set; }

    }
}
