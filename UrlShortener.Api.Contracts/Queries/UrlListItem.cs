﻿using Newtonsoft.Json;
using System;

namespace UrlShortener.Contracts.Queries
{
    public class UrlListItem
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("hasPassword")]
        public bool HasPassword { get; set; }

        [JsonProperty("validUntil")]
        public DateTime? ValidUntil { get; set; }
    }
}
