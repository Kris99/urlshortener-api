﻿using System;
using Newtonsoft.Json;
using UrlShortener.Contracts.Commands.UrlObjects;

namespace UrlShortener.Contracts.Queries
{
    public class UrlDetails
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("validUntil")]
        public DateTime? ValidUntil { get; set; }

        [JsonProperty("hasPassword")]
        public bool HasPassword { get; set; }

        [JsonProperty("method")]
        public string Method { get; set; }

        [JsonProperty("options")]
        public UrlOptions Options { get; set; }
    }
}
