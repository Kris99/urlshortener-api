﻿using Newtonsoft.Json;

namespace UrlShortener.Contracts.Queries
{
    public class Stats
    {
        [JsonProperty("clicks")]
        public int Clicks { get; set; }
    }
}
