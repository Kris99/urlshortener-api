﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace UrlShortener.Contracts.Queries
{
    public class UrlCollection
    {
        [JsonProperty("urls")]
        public IEnumerable<UrlListItem> Urls { get; set; }
    }
}
