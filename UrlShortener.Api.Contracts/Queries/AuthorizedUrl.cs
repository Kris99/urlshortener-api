﻿namespace UrlShortener.Contracts.Queries
{
    public class AuthorizedUrl : UrlDetails
    {
        public bool Authorized { get; set; }

        public string Password { get; set; }
    }
}
