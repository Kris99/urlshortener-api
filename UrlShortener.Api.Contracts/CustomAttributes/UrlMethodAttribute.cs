﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UrlShortener.Contracts.CustomAttributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public class UrlMethodAttribute : DataTypeAttribute
    {
        public UrlMethodAttribute() : base(DataType.Text)
        {
            ErrorMessage = "Method field is invalid. Only possible values: 'GET' and 'POST'";
        }

        public override bool IsValid(object value)
        {
            if (value == null)
                return true;

            switch (value.ToString().ToUpper())
            {
                case "GET": return true;
                case "POST": return true;
                default:
                    return false;
            }
        }
    }
}
