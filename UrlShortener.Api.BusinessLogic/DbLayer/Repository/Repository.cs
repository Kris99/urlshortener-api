﻿using Feratel.Deskline.Base.DatabaseLayer;
using Feratel.Deskline.Infrastructure.ErrorHandling.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using UrlShortener.BusinessLogic.DbLayer.DTO;
using UrlShortener.BusinessLogic.DbLayer.Repository.Interfaces;

namespace UrlShortener.BusinessLogic.DbLayer.Repository
{
    public class Repository : IRepository
    {
        private readonly Database database;

        public Repository(Database database)
        {
            this.database = database;
        }

        public Guid InsertUrl(UrlDTO url)
        {
            string sql =
                @"INSERT INTO dbo.tabUrl (
                     urlIdentity,
                     urlCode,
                     urlValidUntil,
                     urlPassword,
                     urlUrl,
                     urlMethod,
                     urlOptions,
                     urlChangeDate,
                     urlCreateDate
                )
                VALUES (
                    @Identity,
                    @Code,
                    @ValidUntil,
                    @Password,
                    @Url,
                    @Method,
                    @Options,
                    GETDATE(),
                    GETDATE()
                )";

            Guid id = Guid.NewGuid();

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);
                database.AddInParameter(cmd, "@Code", DbType.String, url.Code);
                database.AddInParameter(cmd, "@ValidUntil", DbType.DateTime, url.ValidUntil);
                database.AddInParameter(cmd, "@Password", DbType.String, url.Password);
                database.AddInParameter(cmd, "@Url", DbType.String, url.OriginalUrl);
                database.AddInParameter(cmd, "@Method", DbType.String, url.Method);
                database.AddInParameter(cmd, "@Options", DbType.String, url.Options);

                database.ExecuteNonQuery(cmd);

                return id;
            }
        }

        public void InsertStat(StatDTO stat)
        {
            string sql =
                @"INSERT INTO dbo.tabUrlStatistics (
                    ursUrlID,
                    ursReferer,
                    ursUserAgent,
                    ursDate,
                    ursMaskedIp
                )
                VALUES (
                    @UrlID,
                    @Referer,
                    @UserAgent,
                    GETDATE(),
                    @MaskedIp
                )";

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(cmd, "@UrlID", DbType.Guid, stat.UrlId);
                database.AddInParameter(cmd, "@Referer", DbType.String, stat.Referer);
                database.AddInParameter(cmd, "@UserAgent", DbType.String, stat.UserAgent);
                database.AddInParameter(cmd, "@MaskedIp", DbType.String, stat.MaskedIp);

                database.ExecuteNonQuery(cmd);
            }
        }

        public UrlDTO GetUrlDTOById(Guid id)
        {
            string sql =
                @"SELECT
                    urlIdentity,
                    urlCode,
                    urlUrl,
                    urlValidUntil,
                    urlPassword,
                    urlMethod,
                    urlOptions
                 FROM dbo.tabUrl
                 WHERE urlIdentity = @Identity";

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);

                using (IDataReader dr = database.ExecuteReader(cmd))
                {
                    if (!dr.Read())
                        throw new ApiException("Couldnt find an url with this id.", new ArgumentNullException());

                    return new UrlDTO()
                    {
                        Identity = (Guid)dr["urlIdentity"],
                        Code = dr["urlCode"].ToString(),
                        OriginalUrl = dr["urlUrl"].ToString(),
                        ValidUntil = (DateTime)dr["urlValidUntil"],
                        Password = dr["urlPassword"].ToString(),
                        Method = dr["urlMethod"].ToString(),
                        Options = dr["urlOptions"].ToString()
                    };

                }
            }
        }

        public UrlDTO GetUrlDTOByCode(string code)
        {
            string sql =
                @"SELECT
                    urlIdentity,
                    urlCode,
                    urlValidUntil,
                    urlPassword,
                    urlUrl,
                    urlMethod,
                    urlOptions
                  FROM dbo.tabUrl
                  WHERE urlCode = @Code";

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(cmd, "@Code", DbType.String, code);

                using (IDataReader dr = database.ExecuteReader(cmd))
                {
                    if (!dr.Read())
                        throw new ApiException("Couldnt find an url with this code.", new ArgumentNullException());

                    return new UrlDTO()
                    {
                        Identity = (Guid)dr["urlIdentity"],
                        Code = dr["urlCode"].ToString(),
                        ValidUntil = Convert.ToDateTime(dr["urlValidUntil"]),
                        Password = dr["urlPassword"].ToString(),
                        OriginalUrl = dr["urlUrl"].ToString(),
                        Method = dr["urlMethod"].ToString(),
                        Options = dr["urlOptions"].ToString()
                    };
                }
            }
        }

        public Guid GetUrlIdByCode(string code)
        {
            string sql =
                   @"SELECT urlIdentity
                   FROM dbo.tabUrl
                   WHERE urlCode = @Code";

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(cmd, "@Code", DbType.String, code);

                using (IDataReader dr = database.ExecuteReader(cmd))
                {
                    if (!dr.Read())
                        throw new ApiException("Couldnt find an url with this code.", new ArgumentNullException());

                    return (Guid)dr["urlIdentity"];
                }
            }

        }

        public string GetUrlPasswordByCode(string code)
        {
            string sql =
                   @"SELECT urlPassword
                   FROM dbo.tabUrl
                   WHERE urlCode = @Code";

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(cmd, "@Code", DbType.String, code);

                using (IDataReader dr = database.ExecuteReader(cmd))
                {
                    if (!dr.Read())
                        throw new ApiException("Couldnt read an url with this code.", new ArgumentNullException());

                    return dr["urlPassword"].ToString();
                }
            }
        }

        public string GetOriginalUrlByCode(string code)
        {
            string sql =
                @"SELECT urlUrl
                FROM dbo.tabUrl
                WHERE urlCode = @Code";

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(cmd, "@Code", DbType.String, code);

                using (IDataReader dr = database.ExecuteReader(cmd))
                {
                    if (!dr.Read())
                        throw new ApiException("Couldnt find an url with this code.", new ArgumentNullException());

                    return dr["urlUrl"].ToString();
                }
            }
        }

        public int GetClicks(Guid id)
        {
            string sql = $"SELECT COUNT(ursIdentity) AS RedirectCount FROM dbo.tabUrlStatistic WHERE ursUrlID = @UrlID";

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(cmd, "@UrlID", DbType.Guid, id);

                using (IDataReader dr = database.ExecuteReader(cmd))
                {
                    if (!dr.Read())
                        throw new ApiException("Couldnt read statistics with this id.", new ArgumentNullException());

                    return Convert.ToInt32(dr["RedirectCount"]);
                }
            }
        }

        public IEnumerable<UrlDTO> GetAllUrls()
        {
            string sql = @"
                    SELECT urlIdentity,
                           urlCode,
                           urlUrl,
                           urlPassword,
                           urlValidUntil
                    FROM dbo.tabUrl";

            ICollection<UrlDTO> urls = new List<UrlDTO>();

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                using (IDataReader dr = database.ExecuteReader(cmd))
                {

                    while (dr.Read())
                    {
                        UrlDTO dto = new UrlDTO
                        {
                            Identity = (Guid)dr["urlIdentity"],
                            Code = dr["urlCode"].ToString(),
                            OriginalUrl = dr["urlUrl"].ToString(),
                            Password = dr["urlPassword"].ToString(),
                            ValidUntil = (DateTime)dr["urlValidUntil"]
                        };

                        urls.Add(dto);
                    }

                    return urls;
                }
            }
        }

        public string GetMethodByCode(string code)
        {
            string sql = @"SELECT urlMethod
                           FROM dbo.tabUrl
                           WHERE urlCode = @Code";

            string method = string.Empty;

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(cmd, "@Code", DbType.String, code);

                using (IDataReader dr = database.ExecuteReader(cmd))
                {
                    if (!dr.Read())
                        throw new ApiException("Couldnt find an url with this code.", new ArgumentNullException());

                    method = dr["urlMethod"].ToString();
                }
            }

            return method;
        }

        public void UpdateUrl(UrlDTO dto)
        {
            StringBuilder sql = new StringBuilder();

            sql.AppendLine("UPDATE dbo.tabUrl SET");
            if (dto.OriginalUrl != null)
                sql.AppendLine("urlUrl = @Url,");
            if (dto.Code != null)
                sql.AppendLine("urlCode = @Code,");
            if (dto.ValidUntil != null)
                sql.AppendLine("urlValidUntil = @ValidUntil,");
            if (dto.HasPassword)
                sql.AppendLine("urlPassword = @Password,");
            if (dto.Method != null)
                sql.AppendLine("urlMethod = @Method,");
            if (!string.IsNullOrWhiteSpace(dto.Options))
                sql.AppendLine("urlOptions = @Options,");
            sql.AppendLine("urlChangeDate = GETDATE() ");
            sql.AppendLine("WHERE urlIdentity = @Identity");

            using (DbCommand cmd = database.GetSqlStringCommand(sql.ToString()))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, dto.Identity);
                database.AddInParameter(cmd, "Url", DbType.String, dto.OriginalUrl);

                if (dto.Code != null)
                    database.AddInParameter(cmd, "@Code", DbType.String, dto.Code);

                if (dto.ValidUntil != null)
                    database.AddInParameter(cmd, "@ValidUntil", DbType.DateTime, dto.ValidUntil);

                if (dto.HasPassword)
                    database.AddInParameter(cmd, "@Password", DbType.String, dto.Password);

                if (dto.Method != null)
                    database.AddInParameter(cmd, "@Method", DbType.String, dto.Method);

                if (dto.Options != null)
                    database.AddInParameter(cmd, "@Options", DbType.String, dto.Options);


                database.ExecuteNonQuery(cmd);
            }
        }

        public void DeleteUrlById(Guid id)
        {
            string sql = "DELETE FROM dbo.tabUrl WHERE urlIdentity = @Identity";

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);

                database.ExecuteNonQuery(cmd);
            }
        }

        public bool CheckUniqueCode(string code)
        {
            if (code == null)
                return false;

            string sql = "SELECT COUNT(urlCode) AS 'UniqueCodes' FROM dbo.tabUrl WHERE urlCode = @Code";

            int count = 0;

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(cmd, "@Code", DbType.String, code);

                using (IDataReader dr = database.ExecuteReader(cmd))
                {
                    if (dr.Read())
                    {
                        count = (int)dr["UniqueCodes"];
                    }

                    if (count != 0)
                    {
                        return true;
                    }

                    return false;
                }

            }

        }

        public bool CheckIfUrlExists(Guid id)
        {
            string sql = @"SELECT COUNT(urlIdentity) AS Count FROM dbo.tabUrl WHERE urlIdentity = @Identity";

            int count = 0;

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);

                using (IDataReader dr = database.ExecuteReader(cmd))
                {
                    if (dr.Read())
                        count = Convert.ToInt32(dr["Count"]);
                }
            }

            if (count > 0)
                return true;

            return false;
        }

    }

}

