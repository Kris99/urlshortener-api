﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UrlShortener.BusinessLogic.DbLayer.DTO;

namespace UrlShortener.BusinessLogic.DbLayer.Repository.Interfaces
{
    public interface IRepository
    {
        Guid InsertUrl(UrlDTO urlDTO);

        void InsertStat(StatDTO statDTO);

        UrlDTO GetUrlDTOById(Guid id);

        UrlDTO GetUrlDTOByCode(string code);

        string GetOriginalUrlByCode(string code);

        int GetClicks(Guid id);

        IEnumerable<UrlDTO> GetAllUrls();

        void UpdateUrl(UrlDTO dto);

        void DeleteUrlById(Guid id);

        bool CheckUniqueCode(string code);

        Guid GetUrlIdByCode(string code);

        string GetUrlPasswordByCode(string code);

        bool CheckIfUrlExists(Guid id);

        string GetMethodByCode(string code);
    }
}
