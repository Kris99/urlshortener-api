﻿using System.Text.Json;
using System;
using Feratel.Deskline.Infrastructure.ErrorHandling.Core;
using UrlShortener.BusinessLogic.DbLayer.Repository.Interfaces;
using System.Threading.Tasks;
using System.Collections.Generic;
using UrlShortener.BusinessLogic.DbLayer.DTO;
using UrlShortener.BusinessLogic.DbLayer.Factories.Interfaces;
using UrlShortener.BusinessLogic.Services.Interfaces;
using UrlShortener.Contracts.Commands.UrlObjects;
using UrlShortener.Contracts.Commands;
using UrlShortener.Contracts.Queries;

namespace UrlShortener.BusinessLogic.DbLayer.Factories
{
    public class UrlFactory : IUrlFactory
    {
        private readonly IUrlGenerateService urlGenerateService;

        public UrlFactory(IUrlGenerateService urlGenerateService)
        {
            this.urlGenerateService = urlGenerateService;
        }

        public async Task<UrlDTO> CreateUrlDTO(CreateUrl request)
        {
            if (request == null)
                throw new ApiException("The given request is null.", new ArgumentNullException());

            if (!CheckValidity(request))
                throw new ApiException("The validUntil and validDays fields do not match.", new ArgumentException());

            if (request.Method != null)
            {
                request.Method = request.Method.ToUpper();
            }

            return new UrlDTO()
            {
                OriginalUrl = request.Url,
                Code = await urlGenerateService.GenerateCode(request.Code),
                Method = request.Method ??= "GET",
                ValidUntil = request.ValidUntil,
                Password = urlGenerateService.GenerateHashPassword(request.Password),
                Options = SerializeOptions(request.Options)
            };
        }

        public UrlDTO CreateUrlDTO(Guid Id, UpdateUrl request)
        {
            if (request == null)
                throw new ApiException("The given request is null.", new ArgumentNullException());

            if (!CheckValidity(request))
                throw new ApiException("The validUntil and validDays fields do not match.", new ArgumentException());

            if (request.Method != null)
            {
                request.Method = request.Method.ToUpper();
            }

            var httpsLink = request.Url.Substring(0, 8);
            var httpLink = request.Url.Substring(0, 7);

            return new UrlDTO()
            {
                Identity = Id,
                OriginalUrl = request.Url,
                Code = request.Code,
                Method = request.Method,
                ValidUntil = request.ValidUntil,
                ValidDays = request.ValidDays,
                Password = urlGenerateService.GenerateHashPassword(request.Password),
                Options = SerializeOptions(request.Options)
            };
        }

        public UrlDetails CreateResponse(UrlDTO dto)
        {
            var httpsLink = dto.OriginalUrl.Substring(0, 8);
            var httpLink = dto.OriginalUrl.Substring(0, 7);


            //if the link somehow is saved without the http protocol attached to it
            if (httpsLink != "https://" && httpLink != "http://")
            {
                dto.OriginalUrl = "https://" + dto.OriginalUrl;
            }

            return new UrlDetails()
            {
                Id = dto.Id,
                Url = dto.OriginalUrl,
                Code = dto.Code,
                ValidUntil = dto.ValidUntil,
                HasPassword = dto.HasPassword,
                Method = dto.Method,
                Options = DeserializeOptions(dto.Options)
            };
        }

        public AuthorizedUrl CreateAuthorizedUrl(UrlDTO dto)
        {
            var httpsLink = dto.OriginalUrl.Substring(0, 8);
            var httpLink = dto.OriginalUrl.Substring(0, 7);


            //if the link somehow is saved without the http protocol attached to it
            if (httpsLink != "https://" && httpLink != "http://")
            {
                dto.OriginalUrl = "https://" + dto.OriginalUrl;
            }

            return new AuthorizedUrl()
            {
                Id = dto.Id,
                Code = dto.Code,
                Url = dto.OriginalUrl,
                Password = dto.Password,
                Method = dto.Method,
                Options = DeserializeOptions(dto.Options)
            };
        }

        public UrlListItem CreateUrlListItem(UrlDTO dto)
        {
            if (dto == null)
                return null;

            var httpsLink = dto.OriginalUrl.Substring(0, 8);
            var httpLink = dto.OriginalUrl.Substring(0, 7);

            //if the link somehow is saved without the http protocol attached to it
            if (httpsLink != "https://" && httpLink != "http://")
            {
                dto.OriginalUrl = "https://" + dto.OriginalUrl;
            }

            return new UrlListItem()
            {
                Id = dto.Id,
                Code = dto.Code,
                Url = dto.OriginalUrl,
                HasPassword = dto.HasPassword,
                ValidUntil = dto.ValidUntil
            };
        }

        private string SerializeOptions(UrlOptions options)
        {

            if (options != null)
            {
                try
                {
                    return JsonSerializer.Serialize(options);
                }
                catch (Exception ex)
                {
                    throw new ApiException("Wrong parameters are given for options.", ex);
                }
            }

            return "";
        }

        private UrlOptions DeserializeOptions(string options)
        {
            if (string.IsNullOrWhiteSpace(options))
                return new UrlOptions() { Parameters = new Dictionary<string, object>() };

            try
            {
                return JsonSerializer.Deserialize<UrlOptions>(options);
            }
            catch (Exception ex)
            {
                throw new ApiException("Couldn't deserialize options.", ex);
            }
        }

        private bool CheckValidity(CreateUrl request)
        {
            bool givenDate = request.ValidUntil.HasValue;
            int validDays = request.ValidDays;

            //if there are no given validDays there can't be a missmatch in the dates
            if (validDays <= 0)
            {
                //if there are no given days or date we set the default validity to 7 days
                if (!givenDate)
                    request.ValidUntil = DateTime.Now.AddDays(7);

                return true;
            }

            else
            {
                //compare dates when we have received both validDays and validDate
                if (givenDate)
                {
                    var firstDate = request.ValidUntil.Value.ToString("MMMM dd, yyyy");
                    var secondDate = DateTime.Now.AddDays(request.ValidDays).ToString("MMMM dd, yyyy");

                    if (firstDate.CompareTo(secondDate) == 0)
                        return true;
                    else
                        return false;
                }

                //if there are only given validDays we only we need to create the validDate
                else
                {
                    request.ValidUntil = DateTime.Now.AddDays(validDays);
                    return true;
                }
            }
        }

        private bool CheckValidity(UpdateUrl request)
        {
            bool givenDate = request.ValidUntil.HasValue;
            int validDays = request.ValidDays;

            //if there are no given validDays there can't be a missmatch in the dates
            if (validDays <= 0)
            {
                return true;
            }

            else
            {
                //compare dates when we have received both validDays and validDate
                if (givenDate)
                {
                    var firstDate = request.ValidUntil.Value.ToString("MMMM dd, yyyy");
                    var secondDate = DateTime.Now.AddDays(request.ValidDays).ToString("MMMM dd, yyyy");

                    if (firstDate.CompareTo(secondDate) == 0)
                        return true;
                    else
                        return false;
                }

                //if there are only given validDays we only we need to create the validDate
                else
                {
                    request.ValidUntil = DateTime.Now.AddDays(validDays);
                    return true;
                }
            }
        }

    }
}
