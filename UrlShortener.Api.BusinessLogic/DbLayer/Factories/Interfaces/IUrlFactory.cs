﻿using System;
using System.Threading.Tasks;
using UrlShortener.BusinessLogic.DbLayer.DTO;
using UrlShortener.Contracts.Commands;
using UrlShortener.Contracts.Queries;

namespace UrlShortener.BusinessLogic.DbLayer.Factories.Interfaces
{
    public interface IUrlFactory
    {
        Task<UrlDTO> CreateUrlDTO(CreateUrl request);

        UrlDTO CreateUrlDTO(Guid Id, UpdateUrl request);

        UrlDetails CreateResponse(UrlDTO dto);

        AuthorizedUrl CreateAuthorizedUrl(UrlDTO dto);

        UrlListItem CreateUrlListItem(UrlDTO dto);
    }
}
