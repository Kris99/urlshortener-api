﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using UrlShortener.BusinessLogic.DbLayer.DTO;

namespace UrlShortener.BusinessLogic.DbLayer.Factories.Interfaces
{
    public interface IStatFactory
    {
        StatDTO CreateStat(Guid urlId);
    }
}
