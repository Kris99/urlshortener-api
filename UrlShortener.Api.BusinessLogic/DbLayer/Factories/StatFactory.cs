﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using UrlShortener.BusinessLogic.DbLayer.DTO;
using UrlShortener.BusinessLogic.DbLayer.Factories.Interfaces;

namespace UrlShortener.BusinessLogic.DbLayer.Factories
{
    public class StatFactory : IStatFactory
    {
        private readonly IHttpContextAccessor httpAccessor;

        public StatFactory(IHttpContextAccessor httpAccessor)
        {
            this.httpAccessor = httpAccessor;
        }

        public StatDTO CreateStat(Guid urlId)
        {
            string referer = httpAccessor.HttpContext.Request.Headers["Referer"];
            string userAgent = httpAccessor.HttpContext.Request.Headers["User-Agent"];

            return new StatDTO()
            {
                UrlId = urlId,
                Referer = referer ??= "",
                UserAgent = userAgent ??= "",
                MaskedIp = GetMaskedIp(httpAccessor.HttpContext.Connection.RemoteIpAddress.ToString())
            };
        }

        private string GetMaskedIp(string ip)
        {
            if (ip == null)
                return string.Empty;

            if (ip == "::1")
                return ip;

            int index = ip.LastIndexOf(".");

            return ip.Substring(0, index);
        }
    }
}
