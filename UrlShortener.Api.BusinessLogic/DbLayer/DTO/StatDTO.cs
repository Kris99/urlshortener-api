﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UrlShortener.BusinessLogic.DbLayer.DTO
{
    public class StatDTO
    {
        public Guid UrlId { get; set; }
        public string Referer { get; set; }
        public string UserAgent { get; set; }
        public DateTime Date { get; set; }
        public string MaskedIp { get; set; }
    }
}
