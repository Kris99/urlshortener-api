﻿using System;

namespace UrlShortener.BusinessLogic.DbLayer.DTO
{
    public class UrlDTO
    {
        public Guid Identity { get; set; }

        public string Id { get => Identity.ToString(); }

        public string OriginalUrl { get; set; }

        public string Code { get; set; }

        public DateTime? ValidUntil { get; set; }

        public int ValidDays { get; set; }

        public string Method { get; set; }

        public string Password { get; set; }

        public bool HasPassword
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Password))
                    return false;
                else
                    return true;
            }
        }

        public string Options { get; set; }

    }
}
