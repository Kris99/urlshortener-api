﻿using Feratel.Deskline.Base.DatabaseLayer;
using Lamar;
using System;
using UrlShortener.BusinessLogic.DbLayer.Factories;
using UrlShortener.BusinessLogic.DbLayer.Factories.Interfaces;
using UrlShortener.BusinessLogic.DbLayer.Repository;
using UrlShortener.BusinessLogic.DbLayer.Repository.Interfaces;
using UrlShortener.BusinessLogic.General;
using UrlShortener.BusinessLogic.Services;
using UrlShortener.BusinessLogic.Services.Interfaces;

namespace UrlShortener.BusinessLogic
{
    public class UrlServiceRegistry : ServiceRegistry
    {
        public UrlServiceRegistry()
        {
            For<Database>().Use((context) =>
            {
                var config = context.GetInstance<ApplicationConfig>();
                Database database = DatabaseFactory.CreateDatabase("sql", config.ConnectionString, "", "");
                if (!database.CheckDatabaseConectivity())
                    throw new Exception("No connection to the database with these credentials!");
                return database;
            });
            For<IRepository>().Use<Repository>();
            For<IUrlFactory>().Use<UrlFactory>().Singleton();
            For<IStatFactory>().Use<StatFactory>().Singleton();
            For<IUrlRedirectService>().Use<UrlRedirectService>();
            For<IUrlGenerateService>().Use<UrlGenerateService>();
            For<IUrlShortenerService>().Use<UrlShortenerService>();
            For<IUrlStatisticsService>().Use<UrlStatisticsService>();
        }
    }
}
