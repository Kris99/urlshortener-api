﻿using System;
using UrlShortener.BusinessLogic.DbLayer.DTO;
using UrlShortener.BusinessLogic.DbLayer.Factories.Interfaces;
using UrlShortener.BusinessLogic.DbLayer.Repository.Interfaces;
using UrlShortener.BusinessLogic.Services.Interfaces;
using UrlShortener.Contracts.Queries;

namespace UrlShortener.BusinessLogic.Services
{
    public class UrlStatisticsService : IUrlStatisticsService
    {
        private readonly IStatFactory statFactory;
        private readonly IRepository repository;

        public UrlStatisticsService(IStatFactory statFactory, IRepository repository)
        {
            this.statFactory = statFactory;
            this.repository = repository;
        }

        public void InsertStats(Guid urlId)
        {
            StatDTO stat = statFactory.CreateStat(urlId);

            repository.InsertStat(stat);
        }

        public Stats GetClicks(Guid id)
        {
            int count = repository.GetClicks(id);

            return new Stats() { Clicks = count };
        }
    }
}
