﻿using System.Threading.Tasks;
using System;
using System.Linq;
using Feratel.Deskline.Infrastructure.ErrorHandling.Core;
using UrlShortener.BusinessLogic.DbLayer.DTO;
using UrlShortener.BusinessLogic.DbLayer.Factories.Interfaces;
using UrlShortener.BusinessLogic.DbLayer.Repository.Interfaces;
using UrlShortener.BusinessLogic.Services.Interfaces;
using UrlShortener.Contracts.Commands;
using UrlShortener.Contracts.Queries;

namespace UrlShortener.BusinessLogic.Services
{
    public class UrlShortenerService : IUrlShortenerService
    {
        private readonly IUrlFactory urlFactory;
        private readonly IRepository repository;

        public UrlShortenerService(IUrlFactory urlFactory, IRepository repository)
        {
            this.urlFactory = urlFactory;
            this.repository = repository;
        }

        public async Task<Guid> CreateUrl(CreateUrl request)
        {
            UrlDTO dto = await this.urlFactory.CreateUrlDTO(request);

            Guid id = this.repository.InsertUrl(dto);          

            return id;
        }

        public UrlDetails GetUrlById(Guid id)
        {
            UrlDTO dto = this.repository.GetUrlDTOById(id);

            if (dto == null)
                throw new ApiException("Couldnt find the url u were searching for.", new Exception());

            UrlDetails response = this.urlFactory.CreateResponse(dto);

            return response;
        }

        public UrlCollection GetAllUrls()
        {
            var dtos = this.repository.GetAllUrls();

            UrlCollection urlCollection = new UrlCollection()
            {
                Urls = dtos.Select(dto => this.urlFactory.CreateUrlListItem(dto))
            };

            return urlCollection;
        }

        public string GetOriginalUrlByCode(string code)
        {
            return this.repository.GetOriginalUrlByCode(code);
        }

        public void UpdateById(Guid id, UpdateUrl model)
        {
            if(!this.repository.CheckIfUrlExists(id))
                throw new ApiException("Url with this id doesn't exist.", new ArgumentException());

            if (this.repository.CheckUniqueCode(model.Code))
                throw new ApiException("This code is already taken.", new Exception());

            UrlDTO newUrl = this.urlFactory.CreateUrlDTO(id, model);

            this.repository.UpdateUrl(newUrl);
        }

        public void DeleteById(Guid id)
        {
            if (!this.repository.CheckIfUrlExists(id))
                throw new ApiException("Url with this id doesn't exist.", new ArgumentException());

            this.repository.DeleteUrlById(id);
        }

    }
}
