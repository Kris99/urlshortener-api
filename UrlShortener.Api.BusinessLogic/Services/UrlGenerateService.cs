﻿using Feratel.Deskline.Infrastructure.ErrorHandling.Core;
using System;
using System.Threading.Tasks;
using UrlShortener.BusinessLogic.DbLayer.Repository.Interfaces;
using UrlShortener.BusinessLogic.General;
using UrlShortener.BusinessLogic.Services.Interfaces;

namespace UrlShortener.BusinessLogic.Services
{
    public class UrlGenerateService : IUrlGenerateService
    {
        private readonly IRepository repository;

        public UrlGenerateService(IRepository repository)
        {
            this.repository = repository;
        }

        public async Task<string> GenerateCode(string code, int counter)
        {
            if (counter == 3)
                throw new ApiException("Couldn't load a random code. Please try again.", new Exception());

            if (code == null)
            {
                code = string.Empty;

                char[] symbols = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
                var random = new Random();

                int pathLength = Constants.RANDOMIZED_CODE_LENGTH;

                while (code.Length < pathLength)
                {
                    code += symbols[random.Next(symbols.Length)];
                }

                if (repository.CheckUniqueCode(code))
                    return await GenerateCode(null, ++counter);

                return code;
            }

            else
            {
                code = FormatCode(code);

                if (repository.CheckUniqueCode(code))
                    throw new ApiException("This code is already taken please choose another one.", new ArgumentException());

                return code;
            }
        }

        public string GenerateHashPassword(string password)
        {

            if (password != null)
            {
                string salt = BCrypt.Net.BCrypt.GenerateSalt();
                string hashPassword = BCrypt.Net.BCrypt.HashPassword(password, salt);

                return hashPassword;
            }

            return "";
        }

        private string FormatCode(string code)
        {
            return code.Trim().Replace(" ", "-");
        }
    }
}
