﻿using UrlShortener.BusinessLogic.General;

namespace UrlShortener.BusinessLogic.Services
{
    public static class TranslateService
    {
        private static string _lng;

        public static void SetLanguage(string lng)
        {
            _lng = lng;
        }

        public static string TranslatedTitlePage()
        {
            switch (_lng)
            {
                case "en": return Constants.PASSWORD_CONFIRMATION_ENGLISH;
                case "fr": return Constants.PASSWORD_CONFIRMATION_FRENCH;
                case "it": return Constants.PASSWORD_CONFIRMATION_ITALIAN;
                case "de": return Constants.PASSWORD_CONFIRMATION_GERMAN;

                default: return Constants.PASSWORD_CONFIRMATION_ENGLISH;
            }
        }

        public static string TranslatedPassword()
        {
            switch (_lng)
            {
                case "en": return Constants.PASSWORD_ENGLISH;
                case "fr": return Constants.PASSWORD_FRENCH;
                case "it": return Constants.PASSWORD_ITALIAN;
                case "de": return Constants.PASSWORD_GERMAN;

                default: return Constants.PASSWORD_ENGLISH;
            }
        }

        public static string TranslatedOpenLink()
        {
            switch (_lng)
            {
                case "en": return Constants.OPEN_LINK_ENGLISH;
                case "fr": return Constants.OPEN_LINK_FRENCH;
                case "it": return Constants.OPEN_LINK_ITALIAN;
                case "de": return Constants.OPEN_LINK_GERMAN;

                default: return Constants.OPEN_LINK_ENGLISH;
            }
        }

        public static string TranslatedWrongPassword()
        {
            switch (_lng)
            {
                case "en": return Constants.WRONG_PASSWORD_ENGLISH;
                case "fr": return Constants.WRONG_PASSWORD_FRENCH;
                case "it": return Constants.WRONG_PASSWORD_ITALIAN;
                case "de": return Constants.WRONG_PASSWORD_GERMAN;

                default: return Constants.WRONG_PASSWORD_ENGLISH;
            }
        }

        public static string TranslatedEnterPassword()
        {
            switch (_lng)
            {
                case "en": return Constants.ENTER_PASSWORD_ENGLISH;
                case "fr": return Constants.ENTER_PASSWORD_FRENCH;
                case "it": return Constants.ENTER_PASSWORD_ITALIAN;
                case "de": return Constants.ENTER_PASSWORD_GERMAN;

                default: return Constants.ENTER_PASSWORD_ENGLISH;
            }
        }
    }
}
