﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace UrlShortener.BusinessLogic.Services.Interfaces
{
    public interface IUrlGenerateService
    {
        Task<string> GenerateCode(string code, int counter = 0);

        string GenerateHashPassword(string password);
    }
}
