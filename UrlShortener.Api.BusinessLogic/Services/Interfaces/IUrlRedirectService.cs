﻿using UrlShortener.Contracts.Queries;

namespace UrlShortener.BusinessLogic.Services.Interfaces
{
    public interface IUrlRedirectService
    {
        UrlDetails GetUrlByCodeForRedirection(string code);

        AuthorizedUrl GetAuthorizedUrlByCode(string code, string inputPassword);
    }
}
