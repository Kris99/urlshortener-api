﻿using System.Threading.Tasks;
using System;
using UrlShortener.Contracts.Commands;
using UrlShortener.Contracts.Queries;

namespace UrlShortener.BusinessLogic.Services.Interfaces
{
    public interface IUrlShortenerService
    {
        Task<Guid> CreateUrl(CreateUrl model);
        UrlDetails GetUrlById(Guid id);
        string GetOriginalUrlByCode(string code);
        UrlCollection GetAllUrls();
        void UpdateById(Guid id, UpdateUrl model);
        void DeleteById(Guid id);
    }
}
