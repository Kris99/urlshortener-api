﻿using System;
using UrlShortener.Contracts.Queries;

namespace UrlShortener.BusinessLogic.Services.Interfaces
{
    public interface IUrlStatisticsService
    {
        void InsertStats(Guid urlId);

        Stats GetClicks(Guid id);
    }
}
