﻿using Feratel.Deskline.Infrastructure.ErrorHandling.Core;
using System;
using System.Text;
using UrlShortener.BusinessLogic.DbLayer.DTO;
using UrlShortener.BusinessLogic.DbLayer.Factories.Interfaces;
using UrlShortener.BusinessLogic.DbLayer.Repository.Interfaces;
using UrlShortener.BusinessLogic.Services.Interfaces;
using UrlShortener.Contracts.Commands.UrlObjects;
using UrlShortener.Contracts.Queries;

namespace UrlShortener.BusinessLogic.Services
{
    public class UrlRedirectService : IUrlRedirectService
    {
        private readonly IRepository repository;
        private readonly IUrlGenerateService generateService;
        private readonly IUrlFactory urlFactory;

        public UrlRedirectService(IRepository repository, IUrlGenerateService generate, IUrlFactory urlFactory)
        {
            this.repository = repository;
            generateService = generate;
            this.urlFactory = urlFactory;
        }


        public UrlDetails GetUrlByCodeForRedirection(string code)
        {
            UrlDTO dto = repository.GetUrlDTOByCode(code);

            if (dto.ValidUntil < DateTime.Now)
                throw new ApiException("This link's validation has expired.", new ArgumentOutOfRangeException());

            UrlDetails url = urlFactory.CreateResponse(dto);

            if (url.Method == "GET" && url.Options != null)
            {
                if (url.Options.Parameters != null)
                    url.Url = AddParametersToUrl(url.Url, url.Options);
            }

            return url;
        }

        public AuthorizedUrl GetAuthorizedUrlByCode(string code, string inputPassword)
        {
            UrlDTO dto = repository.GetUrlDTOByCode(code);

            AuthorizedUrl url = urlFactory.CreateAuthorizedUrl(dto);

            url.Authorized = AuthorizeUrl(url.Password, inputPassword);

            if (url.Method == "GET" && url.Options != null)
            {
                if (url.Options.Parameters != null)
                    url.Url = AddParametersToUrl(url.Url, url.Options);
            }

            return url;
        }

        private string AddParametersToUrl(string url, UrlOptions options)
        {
            StringBuilder sb = new StringBuilder();

            var counter = 0;

            if (options.Parameters != null)
            {
                foreach (var parameter in options.Parameters)
                {
                    if (counter == 0)
                    {
                        sb.Append('?');
                    }
                    else
                    {
                        sb.Append('&');
                    }

                    sb.Append(parameter.Key + "=" + parameter.Value);

                    counter++;
                }
            }

            url += sb.ToString();

            return url;
        }

        private bool AuthorizeUrl(string dbPasswordHash, string inputPassword)
        {
            return BCrypt.Net.BCrypt.Verify(inputPassword, dbPasswordHash);
        }

    }
}
