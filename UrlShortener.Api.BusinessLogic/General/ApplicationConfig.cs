﻿using Feratel.Deskline.Infrastructure.HealthCheck.Interfaces.DbConnection;
using Feratel.Deskline.Infrastructure.HealthCheck.Interfaces.RabbitMq;
using Feratel.Deskline.Infrastructure.HealthCheck.Interfaces.Custom;
using Feratel.Deskline.Infrastructure.HealthCheck.Interfaces.Redis;
using Feratel.Deskline.Infrastructure.HealthCheck.Interfaces.Build;
using Feratel.Deskline.Infrastructure.DesklineLog.Core.Interfaces;
using Feratel.Deskline.Infrastructure.HealthCheck.Interfaces;
using Feratel.Deskline.Infrastructure.AppMetrics.Interfaces;
using Feratel.Deskline.Infrastructure.HealthCheck.Models;
using UrlShortener.BusinessLogic.General.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.IO;
using System;

namespace UrlShortener.BusinessLogic.General
{
    public class ApplicationConfig : IAppMetricsSettings, ILoggingSettings, IDesklineHealthChecksSettings, IAuthorizationSettings
    {
        public IConfiguration Config { get; }

        public ApplicationConfig(IConfiguration config)
        {
            Config = config;
        }

        public string ConnectionString 
        {
            get
            {
                string connectionString = Config.GetConnectionString("DefaultConnection");
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);
                string projectDirectory = Directory.GetParent(Environment.CurrentDirectory).FullName;
                builder.AttachDBFilename = projectDirectory + "\\UrlShortener.Api.BusinessLogic\\UrlShortener.mdf";
                return builder.ConnectionString;
            }
        }

        #region Log settings
        public LogLevel GetLogLevel()
        {
            return Config.GetSection("DesklineLogging").GetValue<LogLevel>("LogLevel");
        }

        public string GetLogDatabaseConnectionString()
        {
            return ConnectionString;
        }

        public List<KeyValuePair<string, LogLevel>> GetLogFilters()
        {
            var d = Config.GetSection("Logging").GetSection("LogFilters").Get<Dictionary<string, LogLevel>>();
            if (d != null)
            {
                return d.Select(x => new KeyValuePair<string, LogLevel>(x.Key, x.Value)).ToList();
            }
            return null;
        }

        #endregion

        #region AppMetrics settings
        public bool IsActive()
        {
            return Config.GetSection("appMetrics").GetValue<bool>("active");
        }

        // If true there will be an end point exposed that return the app metrics data.
        // This can be used for debug or for fetching app metrics from external application
        public bool ExposeEndpoints()
        {
            return Config.GetSection("appMetrics").GetValue<bool>("exposeEndpoints");
        }

        //if true then the app metrics data will be pushed to influx database
        public bool ReportToInfluxDb()
        {
            return Config.GetSection("appMetrics").GetValue<bool>("reportToInfluxDb");
        }

        public string GetInfluxServerUrl()
        {
            return Config.GetSection("appMetrics").GetValue<string>("influxDbUrl");
        }

        public string GetInfluxDbName()
        {
            // this is the name of the database for current application
            // wiil be created automaticalli if not available
            return Config.GetSection("appMetrics").GetValue<string>("influxDbName");
        }

        public string GetInfluxDbUser()
        {
            return Config.GetSection("appMetrics").GetValue<string>("influxDbUser");
        }

        public string GetInfluxDbPassword()
        {
            return Config.GetSection("appMetrics").GetValue<string>("influxDbPass");
        }

        // defines how long the data will be kept in the database
        public TimeSpan GetInfluxDbRetentionDuration()
        {
            var days = Config.GetSection("appMetrics").GetValue<short>("influxDbRetentionDuration");

            return new TimeSpan(days, 0, 0, 0);
        }
        #endregion

        #region Cashe settings
        //public LocalCacheOptions GetLocalCacheSettings()
        //{
        //    throw new NotImplementedException();
        //}
        //
        //public RemoteCacheOptions GetRemoteCacheSettings()
        //{
        //    throw new NotImplementedException();
        //}

        #endregion

        #region HealthCheck settings
        public Dictionary<string, IDbConnectionSettings> DbConnections
        {
            get
            {
                return new Dictionary<string, IDbConnectionSettings>()
                {
                    {
                        "test_db_connectivity", new DbConnectionSettings()
                        {
                            ConnectionString = ConnectionString
                        }
                    }
                };
            }
            set
            {

            }
        }
        public Dictionary<string, IRedisConnectionSettings> RedisConnections { get; set; }
        public Dictionary<string, ICustomHealthCheckSettings> CustomHealthChecks { get; set; }
        public KeyValuePair<string, IBuildInformation>? BuildInfo
        {
            get
            {
                return new KeyValuePair<string, IBuildInformation>("build_info", new BuildInformation()
                {
                    BuildCommit = Environment.GetEnvironmentVariable("BUILD_COMMIT"),
                    BuildDate = Environment.GetEnvironmentVariable("BUILD_DATE"),
                    BuildVersion = Environment.GetEnvironmentVariable("BUILD_VERSION")
                });
            }
            set
            {

            }
        }
        public KeyValuePair<string, IRabbitMqConnectionSettings>? RabbitMqConnection { get; set; }
        #endregion

        #region Authorization settings
        public string GetAuthorityLocation()
        {
            return Config.GetSection("authentication").GetValue<string>("authority");
        }
        #endregion

    }
}
