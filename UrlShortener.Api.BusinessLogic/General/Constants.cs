﻿using System.Collections.Generic;

namespace UrlShortener.BusinessLogic.General
{
    public static class Constants
    {
        #region Randomized_Code_Length
        public const int RANDOMIZED_CODE_LENGTH = 7;
        #endregion

        #region Security Constant
        public const string DIS_API_SCOPE_ID = "Deskline.UrlShortener.API";

        public const string DIS_API_SECURITY_POLICY_EDIT = "EditPolicy";
        public const string DIS_API_SECURITY_POLICY_VIEW = "ViewPolicy";

        public const string DIS_API_CLAIM_USAPI_ROLE = "usapi.role";

        public const string DIS_API_CONFIGURATION_ROLE_ADMIN = "Admin";
        public const string DIS_API_CONFIGURATION_ROLE_NORMAL = "Normal";
        #endregion

        #region TranslatedMessages
        public const string PASSWORD_CONFIRMATION_ENGLISH = "Password Confirmation";
        public const string PASSWORD_CONFIRMATION_FRENCH = "Confirmation mot de passe";
        public const string PASSWORD_CONFIRMATION_ITALIAN = "Conferma password";
        public const string PASSWORD_CONFIRMATION_GERMAN = "Passwort Bestätigung";

        public const string ENTER_PASSWORD_ENGLISH = "Please, type your password because of security reasons";
        public const string ENTER_PASSWORD_FRENCH = "Pour de raison de sécurité, indiquez votre mot de passe.";
        public const string ENTER_PASSWORD_ITALIAN = "Inserisca la sua password per ragioni di sicurezza.";
        public const string ENTER_PASSWORD_GERMAN = "Bitte geben Sie Ihr Passwort aus Sicherheitsgründen ein.";

        public const string WRONG_PASSWORD_ENGLISH = "Wrong password";
        public const string WRONG_PASSWORD_FRENCH = "Mot de pass invalide";
        public const string WRONG_PASSWORD_ITALIAN = "Password errata";
        public const string WRONG_PASSWORD_GERMAN = "Passwort falsch";

        public const string PASSWORD_ENGLISH = "Password";
        public const string PASSWORD_FRENCH = "Mot de passe";
        public const string PASSWORD_ITALIAN = "Password";
        public const string PASSWORD_GERMAN = "Passwort";

        public const string OPEN_LINK_ENGLISH = "Open link";
        public const string OPEN_LINK_FRENCH = "Ouvrir lien";
        public const string OPEN_LINK_ITALIAN = "Apri link";
        public const string OPEN_LINK_GERMAN = "Link öffnen";
        #endregion
    }
}
