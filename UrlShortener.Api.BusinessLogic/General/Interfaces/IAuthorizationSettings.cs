﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UrlShortener.BusinessLogic.General.Interfaces
{
    public interface IAuthorizationSettings
    {
        string GetAuthorityLocation();
    }
}
