#UrlShortener.API

This is the description of the UrlShortener API.

How to use it
1. Install the  UrlShortener.Client package
2. In Startup.cs include the following namespaces: 

using UrlShortener.Api.Client;

using UrlShortener.Api.Client.Interfaces;

In Startup.cs use the extention method in ConfigureServices method.

	public class Startup
	{
		public void ConfigureServices(IServiceCollection services)
		{
        		...
			AppConfig config = new AppConfig();

			services.AddSingletion<IUrlShortenerConfiguration>((s) => config);

			services.AddUrlShortener(config);
            	...
		}
	}

This will register for DI an UrlShortener client.

4. How to use it in a service

using UrlShortener.Api.Client.Interfaces;

	public class SomeService
	{
    	private IUrlShortenerService _urlShortenerService;

    	public SomeService(IUrlShortenerService urlShortenerService)
    	{        
    	    this._urlShortenerService = urlShortenerService;
    	}
	
    	public async Task SomeMethod()
    	{
    	    Guid urlId = Guid.Parse("A7E7A999-830B-45B4-84F7-54AF17C0DF48");
    	        
    	    //Identity bearer token
    	    string token = "";
	
    	    UrlDetails url = await this._urlShortenerService.GetUrlDetails(urlId, token);
    	}
	}

