﻿using Feratel.Deskline.Base.DatabaseLayer;
using UrlShortener.BusinessLogic.DbLayer.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data;
using System.Data.Common;
using UrlShortener.BusinessLogic.DbLayer.DTO;

namespace UrlShortener.BusinessLogic.UnitTests.RepositoryTests
{
    [TestClass]
    public class Check_Should
    {
        private string connectionString = new ConfigurationBuilder()
                     .AddJsonFile("appsettings.Development.json")
                     .Build()
                     .GetConnectionString("DefaultConnection");
        private string insertUrl = @"INSERT INTO dbo.tabUrl (
                     urlIdentity,
                     urlCode,
                     urlValidUntil,
                     urlPassword,
                     urlUrl,
                     urlMethod,
                     urlOptions,
                     urlChangeDate,
                     urlCreateDate
                )
                VALUES (
                    @Identity,
                    @Code,
                    @ValidUntil,
                    @Password,
                    @Url,
                    @Method,
                    @Options,
                    GETDATE(),
                    GETDATE()
                )";
        private string clearUrl = "DELETE FROM tabUrl WHERE urlIdentity = @Identity";

        [TestMethod]
        public void CheckUniqueCode_Should_ReturnTrue()
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            Guid id = Guid.NewGuid();
            string code = Guid.NewGuid().ToString();

            UrlDTO url = new UrlDTO()
            {
                Identity = id,
                Code = code,
                OriginalUrl = "https://youtube.com",
                ValidUntil = DateTime.Now,
                Password = "",
                Method = "",
                Options = ""
            };

            using (DbCommand cmd = database.GetSqlStringCommand(insertUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);
                database.AddInParameter(cmd, "@Code", DbType.String, code);
                database.AddInParameter(cmd, "@ValidUntil", DbType.DateTime, url.ValidUntil);
                database.AddInParameter(cmd, "@Password", DbType.String, url.Password);
                database.AddInParameter(cmd, "@Url", DbType.String, url.OriginalUrl);
                database.AddInParameter(cmd, "@Method", DbType.String, url.Method);
                database.AddInParameter(cmd, "@Options", DbType.String, url.Options);

                database.ExecuteNonQuery(cmd);
            }
            #endregion

            #region Act
            var sut = new Repository(database);

            bool result = sut.CheckUniqueCode(code);
            #endregion

            #region Assert
            Assert.IsTrue(result);

            //clearing
            using (DbCommand cmd = database.GetSqlStringCommand(clearUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);

                database.ExecuteNonQuery(cmd);
            }
            #endregion
        }

        [TestMethod]
        public void CheckUniqueCode_Should_ReturnFalse()
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            Guid id = Guid.NewGuid();
            string code = Guid.NewGuid().ToString();

            UrlDTO url = new UrlDTO()
            {
                Identity = id,
                Code = code,
                OriginalUrl = "https://youtube.com",
                ValidUntil = DateTime.Now,
                Password = "",
                Method = "",
                Options = ""
            };

            using (DbCommand cmd = database.GetSqlStringCommand(insertUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);
                database.AddInParameter(cmd, "@Code", DbType.String, code);
                database.AddInParameter(cmd, "@ValidUntil", DbType.DateTime, url.ValidUntil);
                database.AddInParameter(cmd, "@Password", DbType.String, url.Password);
                database.AddInParameter(cmd, "@Url", DbType.String, url.OriginalUrl);
                database.AddInParameter(cmd, "@Method", DbType.String, url.Method);
                database.AddInParameter(cmd, "@Options", DbType.String, url.Options);

                database.ExecuteNonQuery(cmd);
            }
            #endregion

            #region Act
            var sut = new Repository(database);

            bool result = sut.CheckUniqueCode(code + 'a');
            #endregion

            #region Assert
            Assert.IsFalse(result);

            //clearing
            using (DbCommand cmd = database.GetSqlStringCommand(clearUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);

                database.ExecuteNonQuery(cmd);
            }
            #endregion
        }

        [TestMethod]
        public void CheckIfUrlExists_Should_Return_True()
        {
            #region Arrange 
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            Guid id = Guid.NewGuid();
            string code = Guid.NewGuid().ToString();

            UrlDTO url = new UrlDTO()
            {
                Identity = id,
                Code = code,
                OriginalUrl = "https://youtube.com",
                ValidUntil = DateTime.Now,
                Password = "",
                Method = "",
                Options = ""
            };

            using (DbCommand cmd = database.GetSqlStringCommand(insertUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);
                database.AddInParameter(cmd, "@Code", DbType.String, code);
                database.AddInParameter(cmd, "@ValidUntil", DbType.DateTime, url.ValidUntil);
                database.AddInParameter(cmd, "@Password", DbType.String, url.Password);
                database.AddInParameter(cmd, "@Url", DbType.String, url.OriginalUrl);
                database.AddInParameter(cmd, "@Method", DbType.String, url.Method);
                database.AddInParameter(cmd, "@Options", DbType.String, url.Options);

                database.ExecuteNonQuery(cmd);
            }
            #endregion

            #region Act
            var sut = new Repository(database);

            bool result = sut.CheckIfUrlExists(id);
            #endregion

            #region Assert
            Assert.IsTrue(result);

            //clearing
            using (DbCommand cmd = database.GetSqlStringCommand(clearUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);

                database.ExecuteNonQuery(cmd);
            }
            #endregion
        }

        [TestMethod]
        public void CheckIfUrlExists_Should_Return_False()
        {
            #region Arrange 
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            Guid id = Guid.NewGuid();
            string code = Guid.NewGuid().ToString();

            UrlDTO url = new UrlDTO()
            {
                Identity = id,
                Code = code,
                OriginalUrl = "https://youtube.com",
                ValidUntil = DateTime.Now,
                Password = "",
                Method = "",
                Options = ""
            };

            using (DbCommand cmd = database.GetSqlStringCommand(insertUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);
                database.AddInParameter(cmd, "@Code", DbType.String, code);
                database.AddInParameter(cmd, "@ValidUntil", DbType.DateTime, url.ValidUntil);
                database.AddInParameter(cmd, "@Password", DbType.String, url.Password);
                database.AddInParameter(cmd, "@Url", DbType.String, url.OriginalUrl);
                database.AddInParameter(cmd, "@Method", DbType.String, url.Method);
                database.AddInParameter(cmd, "@Options", DbType.String, url.Options);

                database.ExecuteNonQuery(cmd);
            }
            #endregion

            #region Act
            var sut = new Repository(database);

            bool result = sut.CheckIfUrlExists(Guid.NewGuid());
            #endregion

            #region Assert
            Assert.IsFalse(result);

            //clearing
            using (DbCommand cmd = database.GetSqlStringCommand(clearUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);

                database.ExecuteNonQuery(cmd);
            }
            #endregion
        }
    }
}
