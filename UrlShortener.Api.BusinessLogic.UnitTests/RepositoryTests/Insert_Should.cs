﻿using Feratel.Deskline.Base.DatabaseLayer;
using UrlShortener.BusinessLogic.DbLayer.Repository;
using UrlShortener.Contracts.Commands.UrlObjects;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text.Json;
using UrlShortener.BusinessLogic.DbLayer.DTO;

namespace UrlShortener.BusinessLogic.UnitTests.RepositoryTests
{
    [TestClass]
    public class Insert_Should
    {

        private string connectionString = new ConfigurationBuilder()
                                        .AddJsonFile("appsettings.Development.json")
                                        .Build()
                                        .GetConnectionString("DefaultConnection");

        private string clearTabUrl = "DELETE FROM tabUrl WHERE urlIdentity = @Identity";

        [TestMethod]
        [DataRow("https://sportal.bg", "2021-03-05", "1234")]
        [DataRow("https://artgallery.com", "2022-04-06", "1234abcd")]
        [DataRow("https://ciela.bg", "2023-06-07", "0000")]
        public void InsertUrl_Should_InsertTheUrl(string originalUrl, string date, string password)
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            var options = new UrlOptions()
            {
                Parameters = new Dictionary<string, object>()
                {
                    { "length", 21},
                    { "fragments", true}
                }
            };

            DateTime datetime = Convert.ToDateTime(date);

            UrlDTO dto = new UrlDTO()
            {
                Code = Guid.NewGuid().ToString(),
                OriginalUrl = originalUrl,
                ValidUntil = datetime,
                Password = password,
                Method = "POST",
                Options = JsonSerializer.Serialize(options)
            };
            #endregion

            #region Act
            var sut = new Repository(database);

            Guid id = sut.InsertUrl(dto);
            #endregion

            #region Assert
            string sql = "SELECT urlCode FROM tabUrl WHERE urlIdentity = @Identity";

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);

                using (IDataReader dr = database.ExecuteReader(cmd))
                {
                    while (dr.Read())
                    {
                        string dbCode = dr["urlCode"].ToString();

                        Assert.AreEqual(dto.Code, dbCode);
                    }
                }
            }
            #endregion

            #region ClearData
            using (DbCommand cmd = database.GetSqlStringCommand(clearTabUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);

                database.ExecuteNonQuery(cmd);
            }
            #endregion
        }

        [TestMethod]
        [DataRow("sport", null, "2021-03-05", "1234")]
        [DataRow(null, "https://artgallery.com", "2022-04-06", "1234abcd")]
        [DataRow("code", "https://codeacademy.com", "2023-03-04", null)]
        [ExpectedException(typeof(SqlException))]
        public void InsertUrl_Should_ThrowAnException(string code, string originalUrl, string date, string password)
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            var options = new UrlOptions()
            {
                Parameters = new Dictionary<string, object>()
                {
                    { "length", 21},
                    { "fragments", true}
                }
            };

            DateTime? datetime = Convert.ToDateTime(date);

            UrlDTO dto = new UrlDTO()
            {
                Code = code,
                OriginalUrl = originalUrl,
                ValidUntil = datetime,
                Password = password,
                Method = "POST",
                Options = JsonSerializer.Serialize(options)
            };
            #endregion

            #region Act
            var sut = new Repository(database);

            Guid id = sut.InsertUrl(dto);
            #endregion

            #region Assert
            Assert.Fail();
            #endregion

            #region ClearData
            using (DbCommand cmd = database.GetSqlStringCommand(clearTabUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);

                database.ExecuteNonQuery(cmd);
            }
            #endregion

        }

        [TestMethod]
        public void InsertStat_Should_InsertTheStat()
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            Guid urlId = Guid.NewGuid();
            string insertUrl = @"INSERT INTO tabUrl(urlIdentity, urlCode, urlUrl, urlPassword, urlValidUntil, urlMethod, urlOptions, urlChangeDate, urlCreateDate)
                                VALUES (@Identity, '', '', '', '', '', '', GETDATE(), GETDATE())";

            using (DbCommand cmd = database.GetSqlStringCommand(insertUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, urlId);

                database.ExecuteNonQuery(cmd);
            }

            StatDTO stat = new StatDTO()
            {
                UrlId = urlId,
                Referer = "http://localhost:5001",
                UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36",
                MaskedIp = "176.042.568"
            };
            #endregion

            #region Act
            var sut = new Repository(database);

            sut.InsertStat(stat);
            #endregion

            #region Assert
            string sql = "SELECT ursUrlID, ursReferer, ursUserAgent, ursDate, ursMaskedIp FROM tabUrlStatistic WHERE ursUrlID = @UrlID";

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(cmd, "@UrlID", DbType.Guid, urlId);

                using (IDataReader dr = database.ExecuteReader(cmd))
                {
                    if (dr.Read())
                    {
                        Assert.AreEqual(stat.UrlId, (Guid)dr["ursUrlID"]);
                        Assert.AreEqual(stat.Referer, dr["ursReferer"].ToString());
                        Assert.AreEqual(stat.UserAgent, dr["ursUserAgent"].ToString());
                        Assert.AreEqual(stat.MaskedIp, dr["ursMaskedIp"].ToString());
                    }
                }
            }

            using (DbCommand cmd = database.GetSqlStringCommand(clearTabUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, urlId);

                database.ExecuteNonQuery(cmd);
            }
            #endregion
        }

        [TestMethod]
        [DataRow("referer", null, "maskedIp")]
        [DataRow(null, "userAgent", "maskedIp")]
        [DataRow("referer", "userAgent", null)]
        [ExpectedException(typeof(SqlException))]
        public void InsertStat_Should_ThrowAnException(string referer, string userAgent, string maskedIp)
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            Guid urlId = Guid.NewGuid();

            StatDTO stat = new StatDTO()
            {
                UrlId = urlId,
                Referer = referer,
                UserAgent = userAgent,
                MaskedIp = maskedIp
            };
            #endregion

            #region Act
            var sut = new Repository(database);

            sut.InsertStat(stat);
            #endregion

            #region Assert
            Assert.Fail();
            #endregion
        }


    }
}
