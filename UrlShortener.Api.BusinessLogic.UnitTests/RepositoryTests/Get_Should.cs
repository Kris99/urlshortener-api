﻿using Feratel.Deskline.Base.DatabaseLayer;
using Feratel.Deskline.Infrastructure.ErrorHandling.Core;
using UrlShortener.BusinessLogic.DbLayer.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data;
using System.Data.Common;
using UrlShortener.BusinessLogic.DbLayer.DTO;

namespace UrlShortener.BusinessLogic.UnitTests.RepositoryTests
{
    [TestClass]
    public class Get_Should
    {
        private string connectionString = new ConfigurationBuilder()
                                       .AddJsonFile("appsettings.Development.json")
                                       .Build()
                                       .GetConnectionString("DefaultConnection");

        private string clearUrl = "DELETE FROM tabUrl WHERE urlIdentity = @Identity";
        private string insertUrl = @"INSERT INTO dbo.tabUrl (
                     urlIdentity,
                     urlCode,
                     urlValidUntil,
                     urlPassword,
                     urlUrl,
                     urlMethod,
                     urlOptions,
                     urlChangeDate,
                     urlCreateDate
                )
                VALUES (
                    @Identity,
                    @Code,
                    @ValidUntil,
                    @Password,
                    @Url,
                    @Method,
                    @Options,
                    GETDATE(),
                    GETDATE()
                )";
        private string insertStat = @"INSERT INTO dbo.tabUrlStatistic (
                    ursUrlID,
                    ursReferer,
                    ursUserAgent,
                    ursDate,
                    ursMaskedIp
                )
                VALUES (
                    @UrlID,
                    @Referer,
                    @UserAgent,
                    GETDATE(),
                    @MaskedIp
                )";

        [TestMethod]
        public void GetUrlDTOById_Should_ReturnDTO()
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            Guid id = Guid.NewGuid();

            UrlDTO dto = new UrlDTO()
            {
                Identity = id,
                Code = Guid.NewGuid().ToString(),
                OriginalUrl = "https://testing.com",
                ValidUntil = DateTime.Now,
                Password = "12349019",
                Method = "POST",
                Options = ""
            };

            using (DbCommand cmd = database.GetSqlStringCommand(insertUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);
                database.AddInParameter(cmd, "@Code", DbType.String, dto.Code);
                database.AddInParameter(cmd, "@ValidUntil", DbType.DateTime, dto.ValidUntil);
                database.AddInParameter(cmd, "@Password", DbType.String, dto.Password);
                database.AddInParameter(cmd, "@Url", DbType.String, dto.OriginalUrl);
                database.AddInParameter(cmd, "@Method", DbType.String, dto.Method);
                database.AddInParameter(cmd, "@Options", DbType.String, dto.Options);

                database.ExecuteNonQuery(cmd);
            }
            #endregion

            #region Act
            var sut = new Repository(database);

            UrlDTO result = sut.GetUrlDTOById(id);
            #endregion

            #region Assert
            Assert.AreEqual(dto.Identity, result.Identity);
            Assert.AreEqual(dto.OriginalUrl, result.OriginalUrl);
            Assert.AreEqual(dto.Code, result.Code);
            Assert.AreEqual(dto.ValidUntil.Value.ToShortDateString(), result.ValidUntil.Value.ToShortDateString());
            Assert.AreEqual(dto.Password, result.Password);
            Assert.AreEqual(dto.Method, result.Method);
            Assert.AreEqual(dto.Options, result.Options);

            //clearing the table
            using (DbCommand cmd = database.GetSqlStringCommand(clearUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);

                database.ExecuteNonQuery(cmd);
            }
            #endregion

        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void GetUrlDTOById_Should_ThrowException()
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            Guid id = Guid.NewGuid();
            #endregion

            #region Act
            var sut = new Repository(database);

            sut.GetUrlDTOById(id);
            #endregion

            #region Assert
            Assert.Fail("Couldnt find an url with this id.");
            #endregion
        }

        [TestMethod]
        public void GetUrlDTOByCode()
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            Guid id = Guid.NewGuid();

            UrlDTO dto = new UrlDTO()
            {
                Identity = id,
                Code = Guid.NewGuid().ToString(),
                OriginalUrl = "https://testing.com",
                ValidUntil = DateTime.Now,
                Password = "12349019",
                Method = "POST",
                Options = ""
            };

            using (DbCommand cmd = database.GetSqlStringCommand(insertUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);
                database.AddInParameter(cmd, "@Code", DbType.String, dto.Code);
                database.AddInParameter(cmd, "@ValidUntil", DbType.DateTime, dto.ValidUntil);
                database.AddInParameter(cmd, "@Password", DbType.String, dto.Password);
                database.AddInParameter(cmd, "@Url", DbType.String, dto.OriginalUrl);
                database.AddInParameter(cmd, "@Method", DbType.String, dto.Method);
                database.AddInParameter(cmd, "@Options", DbType.String, dto.Options);

                database.ExecuteNonQuery(cmd);
            }
            #endregion

            #region Act
            var sut = new Repository(database);

            UrlDTO result = sut.GetUrlDTOByCode(dto.Code);
            #endregion

            #region Assert
            Assert.AreEqual(dto.Identity, result.Identity);
            Assert.AreEqual(dto.OriginalUrl, result.OriginalUrl);
            Assert.AreEqual(dto.Code, result.Code);
            Assert.AreEqual(dto.ValidUntil.Value.ToShortDateString(), result.ValidUntil.Value.ToShortDateString());
            Assert.AreEqual(dto.Password, result.Password);
            Assert.AreEqual(dto.Method, result.Method);
            Assert.AreEqual(dto.Options, result.Options);

            //clearing the table
            using (DbCommand cmd = database.GetSqlStringCommand(clearUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);

                database.ExecuteNonQuery(cmd);
            }
            #endregion
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void GetUrlDTOByCode_Should_ThrowException()
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            string code = Guid.NewGuid().ToString();
            #endregion

            #region Act
            var sut = new Repository(database);

            sut.GetUrlDTOByCode(code);
            #endregion

            #region Assert
            Assert.Fail("Couldnt find an url with this code.");
            #endregion
        }

        [TestMethod]
        public void GetUrlIdByCode_Should_ReturnId()
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            Guid id = Guid.NewGuid();

            UrlDTO dto = new UrlDTO()
            {
                Identity = id,
                Code = Guid.NewGuid().ToString(),
                OriginalUrl = "",
                ValidUntil = DateTime.Now,
                Password = "",
                Method = "GET",
                Options = ""
            };

            using (DbCommand cmd = database.GetSqlStringCommand(insertUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);
                database.AddInParameter(cmd, "@Code", DbType.String, dto.Code);
                database.AddInParameter(cmd, "@ValidUntil", DbType.DateTime, dto.ValidUntil);
                database.AddInParameter(cmd, "@Password", DbType.String, dto.Password);
                database.AddInParameter(cmd, "@Url", DbType.String, dto.OriginalUrl);
                database.AddInParameter(cmd, "@Method", DbType.String, dto.Method);
                database.AddInParameter(cmd, "@Options", DbType.String, dto.Options);

                database.ExecuteNonQuery(cmd);
            }
            #endregion

            #region Act
            var sut = new Repository(database);

            Guid result = sut.GetUrlIdByCode(dto.Code);
            #endregion

            #region Assert
            Assert.AreEqual(result, id);

            //clearing
            using (DbCommand cmd = database.GetSqlStringCommand(clearUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);

                database.ExecuteNonQuery(cmd);
            }
            #endregion
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void GetUrlIdByCode_Should_ThrowException()
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            string code = Guid.NewGuid().ToString();
            #endregion

            #region Act
            var sut = new Repository(database);

            sut.GetUrlIdByCode(code);
            #endregion

            #region Assert
            Assert.Fail("Couldnt find an url with this code.");
            #endregion
        }

        [TestMethod]
        public void GetUrlPasswordByCode_Should_ReturnPassword()
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            Guid id = Guid.NewGuid();

            UrlDTO dto = new UrlDTO()
            {
                Identity = id,
                Code = Guid.NewGuid().ToString(),
                OriginalUrl = "https://testing.com",
                ValidUntil = DateTime.Now,
                Password = "12349019",
                Method = "POST",
                Options = ""
            };

            using (DbCommand cmd = database.GetSqlStringCommand(insertUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);
                database.AddInParameter(cmd, "@Code", DbType.String, dto.Code);
                database.AddInParameter(cmd, "@ValidUntil", DbType.DateTime, dto.ValidUntil);
                database.AddInParameter(cmd, "@Password", DbType.String, dto.Password);
                database.AddInParameter(cmd, "@Url", DbType.String, dto.OriginalUrl);
                database.AddInParameter(cmd, "@Method", DbType.String, dto.Method);
                database.AddInParameter(cmd, "@Options", DbType.String, dto.Options);

                database.ExecuteNonQuery(cmd);
            }
            #endregion

            #region Act
            var sut = new Repository(database);

            string result = sut.GetUrlPasswordByCode(dto.Code);
            #endregion

            #region Assert
            Assert.AreEqual(dto.Password, result);

            //clearing the table
            using (DbCommand cmd = database.GetSqlStringCommand(clearUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);

                database.ExecuteNonQuery(cmd);
            }
            #endregion
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void GetUrlPasswordByCode_Should_ThrowException()
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            string code = Guid.NewGuid().ToString();
            #endregion

            #region Act
            var sut = new Repository(database);

            sut.GetUrlPasswordByCode(code);
            #endregion

            #region Assert
            Assert.Fail("Couldnt find an url with this code.");
            #endregion
        }

        [TestMethod]
        public void GetOriginalUrlByCode_Should_ReturnOriginalUrl()
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            Guid id = Guid.NewGuid();

            UrlDTO dto = new UrlDTO()
            {
                Identity = id,
                Code = Guid.NewGuid().ToString(),
                OriginalUrl = "https://testing.com",
                ValidUntil = DateTime.Now,
                Password = "12349019",
                Method = "POST",
                Options = ""
            };

            using (DbCommand cmd = database.GetSqlStringCommand(insertUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);
                database.AddInParameter(cmd, "@Code", DbType.String, dto.Code);
                database.AddInParameter(cmd, "@ValidUntil", DbType.DateTime, dto.ValidUntil);
                database.AddInParameter(cmd, "@Password", DbType.String, dto.Password);
                database.AddInParameter(cmd, "@Url", DbType.String, dto.OriginalUrl);
                database.AddInParameter(cmd, "@Method", DbType.String, dto.Method);
                database.AddInParameter(cmd, "@Options", DbType.String, dto.Options);

                database.ExecuteNonQuery(cmd);
            }
            #endregion

            #region Act
            var sut = new Repository(database);

            string result = sut.GetOriginalUrlByCode(dto.Code);
            #endregion

            #region Assert
            Assert.AreEqual(dto.OriginalUrl, result);

            //clearing the table
            using (DbCommand cmd = database.GetSqlStringCommand(clearUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);

                database.ExecuteNonQuery(cmd);
            }
            #endregion
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void GetOriginalUrlByCode_Should_ThrowException()
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            string code = Guid.NewGuid().ToString();
            #endregion

            #region Act
            var sut = new Repository(database);

            sut.GetOriginalUrlByCode(code);
            #endregion

            #region Assert
            Assert.Fail("Couldnt find an url with this code.");
            #endregion
        }

        [TestMethod]
        public void GetClicks_Should_ReturnClicks()
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            Guid urlId = Guid.NewGuid();

            UrlDTO dto = new UrlDTO()
            {
                Identity = urlId,
                Code = Guid.NewGuid().ToString(),
                OriginalUrl = "https://testing.com",
                ValidUntil = DateTime.Now,
                Password = "12349019",
                Method = "POST",
                Options = ""
            };

            //inserting the url
            using (DbCommand cmd = database.GetSqlStringCommand(insertUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, urlId);
                database.AddInParameter(cmd, "@Code", DbType.String, dto.Code);
                database.AddInParameter(cmd, "@ValidUntil", DbType.DateTime, dto.ValidUntil);
                database.AddInParameter(cmd, "@Password", DbType.String, dto.Password);
                database.AddInParameter(cmd, "@Url", DbType.String, dto.OriginalUrl);
                database.AddInParameter(cmd, "@Method", DbType.String, dto.Method);
                database.AddInParameter(cmd, "@Options", DbType.String, dto.Options);

                database.ExecuteNonQuery(cmd);
            }

            StatDTO stat = new StatDTO()
            {
                UrlId = urlId,
                Referer = "http://localhost:5001",
                UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36",
                MaskedIp = "176.054.345"
            };

            var count = new Random().Next(1, 5);
            //inserting  stats
            using (DbCommand cmd = database.GetSqlStringCommand(insertStat))
            {
                database.AddInParameter(cmd, "@UrlID", DbType.Guid, stat.UrlId);
                database.AddInParameter(cmd, "@Referer", DbType.String, stat.Referer);
                database.AddInParameter(cmd, "@UserAgent", DbType.String, stat.UserAgent);
                database.AddInParameter(cmd, "@MaskedIp", DbType.String, stat.MaskedIp);

                for (int i = 0; i < count; i++)
                {
                    database.ExecuteNonQuery(cmd);
                }
            }
            #endregion

            #region Act
            var sut = new Repository(database);

            int clicks = sut.GetClicks(urlId);
            #endregion

            #region Assert
            Assert.AreEqual(clicks, count);

            //clearing the table
            using (DbCommand cmd = database.GetSqlStringCommand(clearUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, urlId);

                database.ExecuteNonQuery(cmd);
            }
            #endregion
        }

        [TestMethod]
        public void GetClicks_Should_ReturnZero()
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            Guid urlId = Guid.NewGuid();
            #endregion

            #region Act
            var sut = new Repository(database);

            var clicks = sut.GetClicks(urlId);
            #endregion

            #region Assert
            Assert.AreEqual(clicks, 0);
            #endregion
        }

        [TestMethod]
        public void GetMethodByCode_Should_ReturnMethod()
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            Guid id = Guid.NewGuid();

            UrlDTO dto = new UrlDTO()
            {
                Identity = id,
                Code = Guid.NewGuid().ToString(),
                OriginalUrl = "https://testing.com",
                ValidUntil = DateTime.Now,
                Password = "12349019",
                Method = "POST",
                Options = ""
            };

            using (DbCommand cmd = database.GetSqlStringCommand(insertUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);
                database.AddInParameter(cmd, "@Code", DbType.String, dto.Code);
                database.AddInParameter(cmd, "@ValidUntil", DbType.DateTime, dto.ValidUntil);
                database.AddInParameter(cmd, "@Password", DbType.String, dto.Password);
                database.AddInParameter(cmd, "@Url", DbType.String, dto.OriginalUrl);
                database.AddInParameter(cmd, "@Method", DbType.String, dto.Method);
                database.AddInParameter(cmd, "@Options", DbType.String, dto.Options);

                database.ExecuteNonQuery(cmd);
            }
            #endregion

            #region Act
            var sut = new Repository(database);

            string method = sut.GetMethodByCode(dto.Code);
            #endregion

            #region Assert
            Assert.AreEqual(dto.Method, method);

            //clearing the table
            using (DbCommand cmd = database.GetSqlStringCommand(clearUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);

                database.ExecuteNonQuery(cmd);
            }
            #endregion
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void GetMethodByCode_Should_ThrowException()
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            string code = Guid.NewGuid().ToString();
            #endregion

            #region Act
            var sut = new Repository(database);

            string method = sut.GetMethodByCode(code);
            #endregion

            #region Assert
            Assert.Fail("Couldnt find an url with this code.");
            #endregion
        }

    }
}
