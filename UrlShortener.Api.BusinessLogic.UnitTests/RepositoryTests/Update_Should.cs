﻿using Feratel.Deskline.Base.DatabaseLayer;
using UrlShortener.BusinessLogic.DbLayer.Repository;
using UrlShortener.Contracts.Commands.UrlObjects;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using UrlShortener.BusinessLogic.DbLayer.DTO;

namespace UrlShortener.BusinessLogic.UnitTests.RepositoryTests
{
    [TestClass]
    public class Update_Should
    {
        private string connectionString = new ConfigurationBuilder()
                                  .AddJsonFile("appsettings.Development.json")
                                  .Build()
                                  .GetConnectionString("DefaultConnection");

        [TestMethod]
        public void UpdateUrl_Should_Update()
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            Guid id = Guid.NewGuid();

            UrlDTO dbUrl = new UrlDTO()
            {
                Identity = id,
                Code = Guid.NewGuid().ToString(),
                OriginalUrl = "https://testing.com",
                ValidUntil = DateTime.Now,
                Password = "12349019",
                Method = "POST",
                Options = "{ parameters: { date: 10/03/2021, browser: Google Chrome } }"
            };

            using (DbCommand cmd = database.GetSqlStringCommand(insertUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);
                database.AddInParameter(cmd, "@Code", DbType.String, dbUrl.Code);
                database.AddInParameter(cmd, "@ValidUntil", DbType.DateTime, dbUrl.ValidUntil);
                database.AddInParameter(cmd, "@Password", DbType.String, dbUrl.Password);
                database.AddInParameter(cmd, "@Url", DbType.String, dbUrl.OriginalUrl);
                database.AddInParameter(cmd, "@Method", DbType.String, dbUrl.Method);
                database.AddInParameter(cmd, "@Options", DbType.String, dbUrl.Options);

                database.ExecuteNonQuery(cmd);
            }

            UrlDTO request = new UrlDTO()
            {
                Identity = id,
                Code = "changed",
                OriginalUrl = "https://twitter.com",
                ValidUntil = DateTime.Now.AddDays(7),
                Password = "", //shouldnt replace it
                Method = "GET",
                Options = "" //shouldnt replace it
            };
            #endregion

            #region Act
            var sut = new Repository(database);

            sut.UpdateUrl(request);

            using (DbCommand cmd = database.GetSqlStringCommand(getUrl))
            {
                database.AddInParameter(cmd, "@ID", DbType.Guid, id);

                using (IDataReader dr = database.ExecuteReader(cmd))
                {
                    dr.Read();

                    Assert.AreEqual(dr["urlIdentity"].ToString(), request.Id);
                    Assert.AreEqual(dr["urlCode"].ToString(), request.Code);
                    Assert.AreEqual(dr["urlUrl"].ToString(), request.OriginalUrl);
                    Assert.AreEqual(dr["urlValidUntil"].ToString(), request.ValidUntil.ToString());
                    Assert.AreEqual(dr["urlPassword"].ToString(), dbUrl.Password);
                    Assert.AreEqual(dr["urlMethod"].ToString(), request.Method);
                    Assert.AreEqual(dr["urlOptions"].ToString(), dbUrl.Options);
                }

                cmd.CommandText = clearUrl;

                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);

                database.ExecuteNonQuery(cmd);
            }
            #endregion
        }

        private string insertUrl = @"INSERT INTO dbo.tabUrl (
                     urlIdentity,
                     urlCode,
                     urlValidUntil,
                     urlPassword,
                     urlUrl,
                     urlMethod,
                     urlOptions,
                     urlChangeDate,
                     urlCreateDate
                )
                VALUES (
                    @Identity,
                    @Code,
                    @ValidUntil,
                    @Password,
                    @Url,
                    @Method,
                    @Options,
                    GETDATE(),
                    GETDATE()
                )";
        private string clearUrl = "DELETE FROM tabUrl WHERE urlIdentity = @Identity";
        private string getUrl = "SELECT urlIdentity, urlCode, urlUrl, urlValidUntil, urlPassword ,urlMethod, urlOptions FROM tabUrl WHERE urlIdentity = @ID";
    }
}
