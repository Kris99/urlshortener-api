﻿using UrlShortener.BusinessLogic.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using UrlShortener.BusinessLogic.DbLayer.Repository.Interfaces;

namespace UrlShortener.BusinessLogic.UnitTests.UrlGenerateServiceTests
{
    [TestClass]
    public class GenerateHashPassword_Should
    {
        [TestMethod]
        public void Should_Return_EmptyString()
        {
            #region Arrange
            var repository = new Mock<IRepository>();
            string password = null;
            #endregion

            #region Act
            var sut = new UrlGenerateService(repository.Object);

            string hashedPassword = sut.GenerateHashPassword(password);
            #endregion

            #region Assert
            Assert.IsTrue(string.IsNullOrEmpty(hashedPassword));
            Assert.AreEqual(hashedPassword, "");
            #endregion

        }

        [TestMethod]
        public void Should_Return_HashedPassword()
        {
            #region Arrange
            var repository = new Mock<IRepository>();
            string password = "1234abcd";
            #endregion

            #region Act
            var sut = new UrlGenerateService(repository.Object);

            string hashedPassword = sut.GenerateHashPassword(password);
            #endregion

            #region Assert
            Assert.IsFalse(string.IsNullOrEmpty(hashedPassword));
            Assert.AreNotEqual(password, hashedPassword);
            #endregion

        }
    }
}
