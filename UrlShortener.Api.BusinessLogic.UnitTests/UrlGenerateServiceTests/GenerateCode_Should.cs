﻿using Feratel.Deskline.Infrastructure.ErrorHandling.Core;
using UrlShortener.BusinessLogic.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UrlShortener.BusinessLogic.DbLayer.Repository.Interfaces;
using UrlShortener.BusinessLogic.General;

namespace UrlShortener.BusinessLogic.UnitTests.UrlGenerateServiceTests
{
    [TestClass]
    public class GenerateCode_Should
    {
        [TestMethod]
        public async Task Should_Return_RandomCode()
        {
            #region Arrange
            var repository = new Mock<IRepository>();

            string code = null;

            repository.Setup(r => r.CheckUniqueCode(code)).Returns(false);
            #endregion

            #region Act
            var sut = new UrlGenerateService(repository.Object);

            string newCode = await sut.GenerateCode(code, 0);
            #endregion

            #region Assert
            Assert.AreEqual(newCode.Length, Constants.RANDOMIZED_CODE_LENGTH);
            #endregion

        }

        [TestMethod]
        [DataRow("health")]
        [DataRow("  health  ")]
        [DataRow("  he al th  ")]
        public async Task Should_Return_CustomCode(string code)
        {
            #region Arrange
            var repository = new Mock<IRepository>();

            repository.Setup(r => r.CheckUniqueCode(code)).Returns(false);
            #endregion

            #region Act
            var sut = new UrlGenerateService(repository.Object);

            string newCode = await sut.GenerateCode(code, 0);
            #endregion

            #region Assert
            Assert.AreEqual(newCode, code.Trim().Replace(" ", "-"));
            #endregion

        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public async Task Should_ThrowExceptionForUniqueUrl()
        {
            #region Arrange
            var repository = new Mock<IRepository>();

            string code = "code";

            repository.Setup(r => r.CheckUniqueCode(code)).Returns(true);
            #endregion

            #region Act
            var sut = new UrlGenerateService(repository.Object);

            string randomCode = await sut.GenerateCode(code, 0);
            #endregion

            #region Assert
            Assert.Fail("This code is already taken please choose another one.");
            #endregion

        }


    }
}
