﻿using Feratel.Deskline.Infrastructure.ErrorHandling.Core;
using UrlShortener.BusinessLogic.Services;
using UrlShortener.Contracts.Commands.UrlObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using UrlShortener.BusinessLogic.DbLayer.DTO;
using UrlShortener.BusinessLogic.DbLayer.Factories.Interfaces;
using UrlShortener.BusinessLogic.DbLayer.Repository.Interfaces;
using UrlShortener.Contracts.Queries;

namespace UrlShortener.BusinessLogic.UnitTests.UrlShortenerServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        [DataRow("sport", "https://sportal.bg", "2021-03-05", null)]
        [DataRow("art", "https://artgallery.com", "2022-04-06", "1234abcd")]
        [DataRow("book", "https://ciela.bg", null, "")]
        public void GetUrlById_Should_ReturnUrl(string code, string originalUrl, string date, string password)
        {
            #region Arrange
            var repository = new Mock<IRepository>();
            var urlFactory = new Mock<IUrlFactory>();
            var options = new UrlOptions()
            {
                Parameters = new Dictionary<string, object>()
                {
                    { "length", 21},
                    { "fragments", true}
                }
            };

            DateTime datetime = Convert.ToDateTime(date);

            UrlDTO dto = new UrlDTO()
            {
                Identity = Guid.NewGuid(),
                Code = code,
                OriginalUrl = originalUrl,
                ValidUntil = datetime,
                Password = password,
                Method = "POST",
                Options = JsonSerializer.Serialize(options)
            };

            UrlDetails response = new UrlDetails()
            {
                Id = dto.Id,
                Code = dto.Code,
                Url = dto.OriginalUrl,
                ValidUntil = dto.ValidUntil,
                HasPassword = dto.HasPassword,
                Method = dto.Method,
                Options = options
            };

            repository.Setup(m => m.GetUrlDTOById(dto.Identity)).Returns(dto);
            urlFactory.Setup(m => m.CreateResponse(dto)).Returns(response);
            #endregion

            #region Act
            var sut = new UrlShortenerService(urlFactory.Object, repository.Object);

            var result = sut.GetUrlById(dto.Identity);
            #endregion

            #region Assert
            Assert.AreEqual(result.Code, response.Code);
            Assert.AreEqual(result.Url, response.Url);
            Assert.AreEqual(result.ValidUntil, response.ValidUntil);
            Assert.AreEqual(result.HasPassword, response.HasPassword);
            Assert.AreEqual(result.Options, response.Options);
            #endregion

        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void GetUrlById_Should_ThrowException()
        {
            #region Arrange
            var repository = new Mock<IRepository>();
            var urlFactory = new Mock<IUrlFactory>();

            Guid id = Guid.NewGuid();

            #endregion

            #region Act
            var sut = new UrlShortenerService(urlFactory.Object, repository.Object);

            sut.GetUrlById(id);
            #endregion

            #region Assert
            Assert.Fail("Couldnt find the url u were searching for.");
            #endregion

        }

        [TestMethod]
        public void GetAllUrls_Should_ReturnCollection()
        {
            #region Arrange
            var repository = new Mock<IRepository>();
            var urlFactory = new Mock<IUrlFactory>();

            var urls = new List<UrlDTO>()
            {
                new UrlDTO() { Identity = Guid.NewGuid(), OriginalUrl = "https://facebook.com" },
                new UrlDTO() { Identity = Guid.NewGuid(), OriginalUrl = "https://instagram.com" }
            };

            UrlListItem urlListItem1 = new UrlListItem()
            {
                Id = urls[0].Id,
                Url = urls[0].OriginalUrl
            };

            UrlListItem urlListItem2 = new UrlListItem()
            {
                Id = urls[1].Id,
                Url = urls[1].OriginalUrl
            };


            repository.Setup(r => r.GetAllUrls()).Returns(urls);
            urlFactory.Setup(f => f.CreateUrlListItem(urls[0])).Returns(urlListItem1);
            urlFactory.Setup(f => f.CreateUrlListItem(urls[1])).Returns(urlListItem2);
            #endregion

            #region Act
            var sut = new UrlShortenerService(urlFactory.Object, repository.Object);

            UrlCollection result = sut.GetAllUrls();
            #endregion

            #region Assert
            Assert.AreEqual(result.Urls.First().Id, urls[0].Id);
            Assert.AreEqual(result.Urls.Last().Id, urls[1].Id);
            Assert.AreEqual(result.Urls.First().Code, urls[0].Code);
            Assert.AreEqual(result.Urls.Last().Code, urls[1].Code);
            #endregion
        }

    }
}
