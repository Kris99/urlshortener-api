﻿using Feratel.Deskline.Infrastructure.ErrorHandling.Core;
using UrlShortener.BusinessLogic.DbLayer.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UrlShortener.BusinessLogic.DbLayer.DTO;
using UrlShortener.BusinessLogic.Services.Interfaces;
using UrlShortener.Contracts.Commands;
using UrlShortener.Contracts.Commands.UrlObjects;
using UrlShortener.Contracts.Queries;

namespace UrlShortener.BusinessLogic.UnitTests.FactoryTests
{
    [TestClass]
    public class UrlFactoryTests
    {
        //CreateUrlDTO with CreateUrl command
        [TestMethod]
        public async Task CreateUrlDTO_Should_ReturnDTO()
        {
            #region Arrange
            var generateService = new Mock<IUrlGenerateService>();

            string password = "080999km";
            string code = "test";
            UrlOptions options = new UrlOptions()
            {
                Parameters = new Dictionary<string, object>()
                {
                    { "browser", "Google Chrome" },
                    { "requestNumber", 23 }
                }
            };

            generateService.Setup(g => g.GenerateHashPassword(password)).Returns("7hSbc72");
            generateService.Setup(g => g.GenerateCode(code, 0).Result).Returns(code);

            CreateUrl request = new CreateUrl()
            {
                Url = "https://test.com",
                Code = code,
                Password = password,
                Method = "GET",
                ValidUntil = DateTime.Now.AddDays(3),
                ValidDays = 3,
                Options = options
            };

            #endregion

            #region Act
            var sut = new UrlFactory(generateService.Object);

            UrlDTO dto = await sut.CreateUrlDTO(request);
            #endregion

            #region Assert
            Assert.AreEqual(dto.Code, request.Code);
            Assert.AreEqual(dto.OriginalUrl, request.Url);
            Assert.AreEqual(dto.Method, request.Method);
            Assert.IsTrue(dto.HasPassword);
            #endregion
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public async Task CreateUrlDTO_Should_ThrowExceptionForValidity()
        {
            #region Arrange
            var generateService = new Mock<IUrlGenerateService>();

            string password = "080999km";
            string code = "test";

            generateService.Setup(g => g.GenerateHashPassword(password)).Returns("7hSbc72");
            generateService.Setup(g => g.GenerateCode(code, 0).Result).Returns(code);

            CreateUrl request = new CreateUrl()
            {
                Url = "https://test.com",
                Code = code,
                Password = password,
                Method = "GET",
                ValidUntil = DateTime.Now.AddDays(3),
                ValidDays = 4
            };

            #endregion

            #region Act
            var sut = new UrlFactory(generateService.Object);

            await sut.CreateUrlDTO(request);
            #endregion

            #region Assert
            Assert.Fail("The validUntil and validDays fields do not match.");
            #endregion
        }


        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public async Task CreateUrlDTO_Should_ThrowExceptionForRequest()
        {
            #region Arrange
            var generateService = new Mock<IUrlGenerateService>();

            CreateUrl request = null;
            #endregion

            #region Act
            var sut = new UrlFactory(generateService.Object);

            await sut.CreateUrlDTO(request);
            #endregion

            #region Assert
            Assert.Fail("The given request is null.");
            #endregion
        }


        //CreateUrlDTO with UpdateUrl command
        [TestMethod]
        public void CreateUrlDto_Should_ReturnDTO()
        {
            #region Arrange
            var generateService = new Mock<IUrlGenerateService>();

            Guid id = Guid.NewGuid();
            string password = "080999km";

            UrlOptions options = new UrlOptions()
            {
                Parameters = new Dictionary<string, object>()
                {
                    { "browser", "Google Chrome" },
                    { "requestNumber", 23 }
                }
            };

            generateService.Setup(g => g.GenerateHashPassword(password)).Returns("7hSbc72");

            UpdateUrl request = new UpdateUrl()
            {
                Url = "https://test.com",
                Code = "test",
                Password = password,
                Method = "GET",
                ValidUntil = DateTime.Now.AddDays(3),
                ValidDays = 3,
                Options = options
            };

            #endregion

            #region Act
            var sut = new UrlFactory(generateService.Object);

            UrlDTO dto = sut.CreateUrlDTO(id, request);
            #endregion

            #region Assert
            Assert.AreEqual(dto.Code, request.Code);
            Assert.AreEqual(dto.OriginalUrl, request.Url);
            Assert.AreEqual(dto.Method, request.Method);
            Assert.IsTrue(dto.HasPassword);
            #endregion
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void CreateUrlDto_Should_ThrowExceptionForValidity()
        {
            #region Arrange
            var generateService = new Mock<IUrlGenerateService>();

            Guid id = Guid.NewGuid();
            string password = "080999km";

            UrlOptions options = new UrlOptions()
            {
                Parameters = new Dictionary<string, object>()
                {
                    { "browser", "Google Chrome" },
                    { "requestNumber", 23 }
                }
            };

            generateService.Setup(g => g.GenerateHashPassword(password)).Returns("7hSbc72");

            UpdateUrl request = new UpdateUrl()
            {
                Url = "https://test.com",
                Code = "test",
                Password = password,
                Method = "GET",
                ValidUntil = DateTime.Now.AddDays(3),
                ValidDays = 5,
                Options = options
            };

            #endregion

            #region Act
            var sut = new UrlFactory(generateService.Object);

            UrlDTO dto = sut.CreateUrlDTO(id, request);
            #endregion

            #region Assert
            Assert.Fail("The validUntil and validDays fields do not match.");
            #endregion
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void CreateUrlDto_Should_ThrowExceptionForRequest()
        {
            #region Arrange
            var generateService = new Mock<IUrlGenerateService>();

            Guid id = Guid.NewGuid();

            UpdateUrl request = null;

            #endregion

            #region Act
            var sut = new UrlFactory(generateService.Object);

            sut.CreateUrlDTO(id, request);
            #endregion

            #region Assert
            Assert.Fail("The given request is null.");
            #endregion
        }

        [TestMethod]
        public void CreateUrlListItem_Should_ReturnUrlListItem()
        {
            #region Arrange
            var generateService = new Mock<IUrlGenerateService>();

            UrlDTO dto = new UrlDTO()
            {
                Identity = Guid.NewGuid(),
                Code = "test",
                OriginalUrl = "https://testing.com",
                Password = "tester123",
                ValidUntil = DateTime.Now.AddDays(1)
            };
            #endregion

            #region Act
            var sut = new UrlFactory(generateService.Object);

            UrlListItem listItem = sut.CreateUrlListItem(dto);
            #endregion

            #region Assert
            Assert.AreEqual(dto.OriginalUrl, listItem.Url);
            Assert.AreEqual(dto.Code, listItem.Code);
            Assert.AreEqual(dto.HasPassword, listItem.HasPassword);
            #endregion
        }

        [TestMethod]
        public void CreateUrlListItem_Should_ReturnNull()
        {
            #region Arrange
            var generateService = new Mock<IUrlGenerateService>();

            UrlDTO dto = null;
            #endregion

            #region Act
            var sut = new UrlFactory(generateService.Object);

            UrlListItem listItem = sut.CreateUrlListItem(dto);
            #endregion

            #region Assert
            Assert.IsNull(listItem);
            #endregion
        }

        [TestMethod]
        public void DeserializeOptions_Should_ReturnOptions()
        {
            #region Arrange
            var generateService = new Mock<IUrlGenerateService>();

            string options = "{\"parameters\":{\"code\":\"test\",\"password\":\"1234abcd\"}}";

            UrlDTO dto = new UrlDTO()
            {
                Identity = Guid.NewGuid(),
                OriginalUrl = "https://youtube.com",
                Code = "test",
                ValidUntil = DateTime.Now.AddDays(7),
                Password = "1234abcd",
                Method = "GET",
                Options = options
            };

            #endregion

            #region Act
            var sut = new UrlFactory(generateService.Object);

            UrlDetails url = sut.CreateResponse(dto);
            #endregion

            #region Assert
            Assert.AreEqual(dto.Id, url.Id);
            Assert.AreEqual(dto.Code, url.Code);
            Assert.AreEqual(dto.OriginalUrl, url.Url);
            Assert.AreEqual(dto.HasPassword, url.HasPassword);
            Assert.AreEqual(dto.Method, url.Method);
            Assert.IsInstanceOfType(url.Options, typeof(UrlOptions));
            #endregion
        }

        [TestMethod]
        [DataRow(null)]
        [DataRow("")]
        [DataRow("    ")]
        public void DeserializeOptions_Should_ReturnNull(string options)
        {
            #region Arrange
            var generateService = new Mock<IUrlGenerateService>();

            UrlDTO dto = new UrlDTO()
            {
                Identity = Guid.NewGuid(),
                OriginalUrl = "https://youtube.com",
                Code = "test",
                ValidUntil = DateTime.Now.AddDays(7),
                Password = "1234abcd",
                Method = "GET",
                Options = options
            };

            #endregion

            #region Act
            var sut = new UrlFactory(generateService.Object);

            UrlDetails url = sut.CreateResponse(dto);
            #endregion

            #region Assert
            Assert.IsTrue(url.Options.Parameters.Count == 0);
            #endregion
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void DeserializeOptions_Should_ThrowException()
        {
            #region Arrange
            var generateService = new Mock<IUrlGenerateService>();

            string options = "{\"parameters\":{\"code\":\"test\", 123 :\"1234abcd\"}}";

            UrlDTO dto = new UrlDTO()
            {
                Identity = Guid.NewGuid(),
                OriginalUrl = "https://youtube.com",
                Code = "test",
                ValidUntil = DateTime.Now.AddDays(7),
                Password = "1234abcd",
                Method = "GET",
                Options = options
            };

            #endregion

            #region Act
            var sut = new UrlFactory(generateService.Object);

            sut.CreateResponse(dto);
            #endregion

            #region Assert
            Assert.Fail("Couldn't deserialize options.");
            #endregion
        }

    }
}
