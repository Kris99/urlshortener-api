﻿using UrlShortener.BusinessLogic.DbLayer.Factories;
using Microsoft.AspNetCore.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using UrlShortener.BusinessLogic.DbLayer.DTO;

namespace UrlShortener.BusinessLogic.UnitTests.FactoryTests
{
    [TestClass]
    public class StatFactoryTests
    {
        [TestMethod]
        public void CreateStat_Should_ReturnStatDTO()
        {
            #region Arrange
            var httpContext = new Mock<IHttpContextAccessor>();

            string referer = "http://localhost:5001";
            string userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36";
            long address = 2343842678;
            string ip = "118.51.180.139";

            IPAddress ipAddress = new IPAddress(address);

            httpContext.Setup(c => c.HttpContext.Request.Headers["Referer"]).Returns(referer);
            httpContext.Setup(c => c.HttpContext.Request.Headers["User-Agent"]).Returns(userAgent);
            httpContext.Setup(c => c.HttpContext.Connection.RemoteIpAddress).Returns(ipAddress);
            #endregion

            #region Act
            var sut = new StatFactory(httpContext.Object);

            StatDTO stat = sut.CreateStat(Guid.NewGuid());
            #endregion

            #region Assert
            Assert.AreEqual(stat.Referer, referer);
            Assert.AreEqual(stat.UserAgent, userAgent);
            Assert.AreEqual(stat.MaskedIp, ip.Substring(0, 10));
            #endregion
        }

    }
}
