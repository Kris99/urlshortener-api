﻿using Feratel.Deskline.Infrastructure.ErrorHandling.Core;
using UrlShortener.BusinessLogic.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using UrlShortener.BusinessLogic.DbLayer.DTO;
using UrlShortener.BusinessLogic.DbLayer.Factories.Interfaces;
using UrlShortener.BusinessLogic.DbLayer.Repository.Interfaces;
using UrlShortener.BusinessLogic.Services.Interfaces;
using UrlShortener.Contracts.Commands.UrlObjects;
using UrlShortener.Contracts.Queries;

namespace UrlShortener.BusinessLogic.UnitTests.UrlRedirectServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public void GetUrlByCodeForRedirection_Should_ReturnUrl()
        {
            #region Arrange
            var repository = new Mock<IRepository>();
            var generateService = new Mock<IUrlGenerateService>();
            var urlFactory = new Mock<IUrlFactory>();

            UrlDTO dto = new UrlDTO()
            {
                Identity = Guid.NewGuid(),
                OriginalUrl = "https://youtube.com",
                Code = "test",
                ValidUntil = DateTime.Now.AddDays(3),
                Password = "1234",
                Method = "GET",
                Options = "{\"parameters\":{\"parameter1\":\"value1\",\"parameter2\":\"value2\"}}"
            };

            UrlOptions options = new UrlOptions()
            {
                Parameters = new Dictionary<string, object>()
                {
                    { "parameter1", "value1" },
                    { "parameter2", "value2" }
                }
            };

            UrlDetails url = new UrlDetails()
            {
                Id = dto.Id,
                Url = dto.OriginalUrl,
                Code = dto.Code,
                ValidUntil = dto.ValidUntil,
                HasPassword = dto.HasPassword,
                Method = dto.Method,
                Options = options
            };

            repository.Setup(r => r.GetUrlDTOByCode(dto.Code)).Returns(dto);
            urlFactory.Setup(f => f.CreateResponse(dto)).Returns(url);
            #endregion

            #region Act
            var sut = new UrlRedirectService(repository.Object, generateService.Object, urlFactory.Object);

            UrlDetails result = sut.GetUrlByCodeForRedirection(dto.Code);
            #endregion

            #region Assert
            Assert.AreEqual(result.Id, dto.Id);
            Assert.AreEqual(result.Method, dto.Method);
            Assert.AreEqual(result.HasPassword, dto.HasPassword);
            Assert.AreEqual(dto.OriginalUrl + "?parameter1=value1&parameter2=value2", result.Url);
            #endregion
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void GetUrlByCodeForRedirection_Should_ThrowExceptionForValidity()
        {
            #region Arrange
            var repository = new Mock<IRepository>();
            var generateService = new Mock<IUrlGenerateService>();
            var urlFactory = new Mock<IUrlFactory>();

            UrlDTO dto = new UrlDTO()
            {
                Identity = Guid.NewGuid(),
                OriginalUrl = "https://youtube.com",
                Code = "test",
                ValidUntil = DateTime.Now,
                Password = "1234",
                Method = "GET",
                Options = "{\"parameters\":{\"parameter1\":\"value1\",\"parameter2\":\"value2\"}}"
            };

            repository.Setup(r => r.GetUrlDTOByCode(dto.Code)).Returns(dto);
            #endregion

            #region Act
            var sut = new UrlRedirectService(repository.Object, generateService.Object, urlFactory.Object);

            UrlDetails result = sut.GetUrlByCodeForRedirection(dto.Code);
            #endregion

            #region Assert
            Assert.Fail("This link's validation has expired.");
            #endregion
        }

        [TestMethod]
        [DataRow("1234", true)]
        [DataRow("123456", false)]
        public void GetAuthorizedUrlByCode_Should_ReturnUrl(string inputPassword, bool expectedResult)
        {
            #region Arrange
            var repository = new Mock<IRepository>();
            var generateService = new Mock<IUrlGenerateService>();
            var urlFactory = new Mock<IUrlFactory>();

            string salt = BCrypt.Net.BCrypt.GenerateSalt();

            UrlDTO dto = new UrlDTO()
            {
                Identity = Guid.NewGuid(),
                OriginalUrl = "https://youtube.com",
                Code = "test",
                ValidUntil = DateTime.Now,
                Password = BCrypt.Net.BCrypt.HashPassword("1234", salt),
                Method = "POST",
                Options = "{\"parameters\":{\"parameter1\":\"value1\",\"parameter2\":\"value2\"}}"
            };


            UrlOptions options = new UrlOptions()
            {
                Parameters = new Dictionary<string, object>()
                {
                    { "parameter1", "value1" },
                    { "parameter2", "value2" }
                }
            };

            AuthorizedUrl url = new AuthorizedUrl()
            {
                Id = dto.Id,
                Url = dto.OriginalUrl,
                Code = dto.Code,
                ValidUntil = dto.ValidUntil,
                HasPassword = dto.HasPassword,
                Method = dto.Method,
                Options = options,
                Password = dto.Password
            };

            repository.Setup(r => r.GetUrlDTOByCode(dto.Code)).Returns(dto);
            urlFactory.Setup(f => f.CreateAuthorizedUrl(dto)).Returns(url);
            #endregion

            #region Act
            var sut = new UrlRedirectService(repository.Object, generateService.Object, urlFactory.Object);

            AuthorizedUrl result = sut.GetAuthorizedUrlByCode(dto.Code, inputPassword);
            #endregion

            #region Assert
            Assert.AreEqual(result.Authorized, expectedResult);
            Assert.AreEqual(result.Url, dto.OriginalUrl);
            Assert.AreEqual(result.Code, dto.Code);
            #endregion
        }

    }
}
