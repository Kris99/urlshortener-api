﻿using UrlShortener.BusinessLogic.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using UrlShortener.BusinessLogic.DbLayer.Factories.Interfaces;
using UrlShortener.BusinessLogic.DbLayer.Repository.Interfaces;
using UrlShortener.Contracts.Queries;

namespace UrlShortener.BusinessLogic.UnitTests.UrlStatisticsServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        [DataRow(1)]
        [DataRow(200)]
        [DataRow(3000)]
        [DataRow(40000)]
        public void GetClicks_Should_Return_Stats(int clicks)
        {
            #region Arrange
            var statFactory = new Mock<IStatFactory>();
            var repository = new Mock<IRepository>();

            Guid id = Guid.NewGuid();

            repository.Setup(r => r.GetClicks(id)).Returns(clicks);
            #endregion

            #region Act
            var sut = new UrlStatisticsService(statFactory.Object, repository.Object);

            Stats stats = sut.GetClicks(id);
            #endregion

            #region Assert
            Assert.AreEqual(stats.Clicks, clicks);
            #endregion
        }
    }
}
