﻿using Feratel.Deskline.Base.DatabaseLayer;
using UrlShortener.BusinessLogic.DbLayer.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data;
using System.Data.Common;
using UrlShortener.BusinessLogic.DbLayer.DTO;

namespace UrlShortener.BusinessLogic.UnitTests.RepositoryServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        private string connectionString = new ConfigurationBuilder()
                             .AddJsonFile("appsettings.Development.json")
                             .Build()
                             .GetConnectionString("DefaultConnection");

        private string insertUrl = @"INSERT INTO dbo.tabUrl (
                     urlIdentity,
                     urlCode,
                     urlValidUntil,
                     urlPassword,
                     urlUrl,
                     urlMethod,
                     urlOptions,
                     urlChangeDate,
                     urlCreateDate
                )
                VALUES (
                    @Identity,
                    @Code,
                    @ValidUntil,
                    @Password,
                    @Url,
                    @Method,
                    @Options,
                    GETDATE(),
                    GETDATE()
                )";
        private string getUrl = "SELECT urlIdentity, urlCode, urlUrl, urlValidUntil, urlPassword ,urlMethod, urlOptions FROM tabUrl WHERE urlIdentity = @Identity";


        [TestMethod]
        public void DeleteById_Should_Delete()
        {
            #region Arrange
            Database database = DatabaseFactory.CreateDatabase("sql", connectionString, "", "");

            Guid id = Guid.NewGuid();

            UrlDTO dto = new UrlDTO()
            {
                Identity = id,
                Code = Guid.NewGuid().ToString(),
                OriginalUrl = "https://testing.com",
                ValidUntil = DateTime.Now,
                Password = "12349019",
                Method = "POST",
                Options = ""
            };

            using (DbCommand cmd = database.GetSqlStringCommand(insertUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);
                database.AddInParameter(cmd, "@Code", DbType.String, dto.Code);
                database.AddInParameter(cmd, "@ValidUntil", DbType.DateTime, dto.ValidUntil);
                database.AddInParameter(cmd, "@Password", DbType.String, dto.Password);
                database.AddInParameter(cmd, "@Url", DbType.String, dto.OriginalUrl);
                database.AddInParameter(cmd, "@Method", DbType.String, dto.Method);
                database.AddInParameter(cmd, "@Options", DbType.String, dto.Options);

                database.ExecuteNonQuery(cmd);
            }
            #endregion

            #region Act
            var sut = new Repository(database);

            sut.DeleteUrlById(id);
            #endregion

            #region Assert
            using (DbCommand cmd = database.GetSqlStringCommand(getUrl))
            {
                database.AddInParameter(cmd, "@Identity", DbType.Guid, id);

                using (IDataReader dr = database.ExecuteReader(cmd))
                {
                    Assert.IsFalse(dr.Read());
                }
            }
            #endregion
        }

    }
}
