﻿using Feratel.Deskline.Infrastructure.HttpClient.Interfaces;
using Feratel.Deskline.Infrastructure.HttpClient.Services;
using UrlShortener.Contracts.Commands;
using UrlShortener.Contracts.Commands.UrlObjects;
using UrlShortener.Contracts.Queries;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UrlShortener.Api.Client.General;
using UrlShortener.Api.Client.Interfaces;

namespace UrlShortener.Api.Client.Services
{
    public class UrlShortenerService : IUrlShortenerService
    {
        #region Constructor

        private readonly IHttpClient<UrlShortenerService> _httpClient;
        private readonly IUrlShortenerConfiguration _config;

        public UrlShortenerService(IHttpClient<UrlShortenerService> httpClient, IUrlShortenerConfiguration config)
        {
            _httpClient = httpClient;
            _config = config;
        }
        #endregion

        #region Common methods
        protected string GetBaseUrl()
        {
            string url = _config.GetUrlShortenerApiBaseUrl();

            return $"{url?.Trim()?.TrimEnd('/')}";
        }

        private List<KeyValuePair<string, string>> GetCustomHeaders(string token)
        {
            var result = new List<KeyValuePair<string, string>>();

            result.Add(new KeyValuePair<string, string>("Authorization", token));

            return result;
        }

        #endregion

        #region HealthCheck Controller
        public async Task<string> GetUrlShortenerHealthCheckStatus()
        {
            string url = $"{GetBaseUrl()}/{Constants.API_ROUTE_HEALTH_CHECK_STATUS}";

            return await _httpClient.Get<string>(url);
        }

        #endregion

        #region UrlShortenerController
        public async Task<UrlDetails> CreateUrl(CreateUrl command, string token)
        {
            string url = $"{GetBaseUrl()}/{Constants.API_GENERAL_ROUTE}";

            return await _httpClient.Post<CreateUrl, UrlDetails>(url, command, GetCustomHeaders(token));
        }

        public async Task<UrlDetails> CreateUrl(string originalUrl, string token, string code = null, DateTime? validUntil = null, int validDays = 0, string password = null, string method = null, UrlOptions options = null)
        {
            string url = $"{GetBaseUrl()}/{Constants.API_GENERAL_ROUTE}";

            CreateUrl command = new CreateUrl()
            {
                Url = originalUrl,
                Code = code,
                ValidUntil = validUntil,
                ValidDays = validDays,
                Password = password,
                Method = method,
                Options = options
            };

            return await _httpClient.Post<CreateUrl, UrlDetails>(url, command, GetCustomHeaders(token));
        }

        public async Task<UrlDetails> GetUrlDetails(Guid id, string token)
        {
            string url = $"{GetBaseUrl()}/{Constants.API_GENERAL_ROUTE}/{id}";

            return await _httpClient.Get<UrlDetails>(url, GetCustomHeaders(token));
        }

        public async Task<UrlCollection> GetAllUrls(string token)
        {
            string url = $"{GetBaseUrl()}/{Constants.API_GET_ALL_URLS_ROUTE}";

            return await _httpClient.Get<UrlCollection>(url, GetCustomHeaders(token));
        }

        public async Task<UrlDetails> UpdateUrl(Guid id, UpdateUrl command, string token)
        {
            string url = $"{GetBaseUrl()}/{Constants.API_GENERAL_ROUTE}/{id}";

            return await _httpClient.Put<UpdateUrl, UrlDetails>(url, command, GetCustomHeaders(token));
        }

        public async Task<UrlDetails> UpdateUrl(Guid id, string originalUrl, string token, string code = null, DateTime? validUntil = null, int validDays = 0, string password = null, string method = null, UrlOptions options = null)
        {
            string url = $"{GetBaseUrl()}/{Constants.API_GENERAL_ROUTE}/{id}";

            UpdateUrl command = new UpdateUrl()
            {
                Url = originalUrl,
                Code = code,
                ValidUntil = validUntil,
                ValidDays = validDays,
                Password = password,
                Method = method,
                Options = options
            };

            return await _httpClient.Put<UpdateUrl, UrlDetails>(url, command, GetCustomHeaders(token));
        }

        public async Task DeleteUrl(Guid id, string token)
        {
            string url = $"{GetBaseUrl()}/{Constants.API_GENERAL_ROUTE}/{id}";

            await _httpClient.Delete(url, GetCustomHeaders(token));
        }
        #endregion

        #region UrlStatisticsController
        public async Task<Stats> GetUrlStats(Guid id, string token)
        {
            string url = $"{GetBaseUrl()}/{Constants.API_STATS_ROUTE}/{id}";

            return await _httpClient.Get<Stats>(url, GetCustomHeaders(token));
        }
        #endregion

    }


}
