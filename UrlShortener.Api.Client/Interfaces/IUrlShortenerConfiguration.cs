﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UrlShortener.Api.Client.Interfaces
{
    public interface IUrlShortenerConfiguration
    {
        string GetUrlShortenerApiBaseUrl();
    }
}
