﻿using UrlShortener.Contracts.Commands;
using UrlShortener.Contracts.Commands.UrlObjects;
using UrlShortener.Contracts.Queries;
using System;
using System.Threading.Tasks;

namespace UrlShortener.Api.Client.Interfaces
{
    public interface IUrlShortenerService
    {
        Task<string> GetUrlShortenerHealthCheckStatus();

        Task<UrlDetails> CreateUrl(CreateUrl command, string token);

        Task<UrlDetails> CreateUrl(string originalUrl, string code = null, string token = null, DateTime? validUntil = null, int validDays = 0, string password = null, string method = null, UrlOptions options = null);

        Task<UrlDetails> GetUrlDetails(Guid id, string token);

        Task<UrlCollection> GetAllUrls(string token);

        Task<Stats> GetUrlStats(Guid id, string token);

        Task<UrlDetails> UpdateUrl(Guid id, UpdateUrl command, string token);

        Task<UrlDetails> UpdateUrl(Guid id, string originalUrl, string token, string code = null, DateTime? validUntil = null, int validDays = 0, string password = null, string method = null, UrlOptions options = null);

        Task DeleteUrl(Guid id, string token);

    }
}
