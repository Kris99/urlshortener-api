﻿namespace UrlShortener.Api.Client.General
{
    public class Constants
    {

        internal static string API_ROUTE_HEALTH_CHECK_STATUS = "health/status";

        internal static string API_GENERAL_ROUTE = "urls";
        internal static string API_GET_ALL_URLS_ROUTE = "urls/list";

        internal static string API_STATS_ROUTE = "stats";
    }
}
