﻿using System;
using System.Collections.Generic;
using System.Text;
using UrlShortener.Api.Client.Interfaces;

namespace UrlShortener.Api.Client
{
    public class UrlShortenerConfiguration : IUrlShortenerConfiguration
    {
        private readonly string _baseUrl;

        public UrlShortenerConfiguration(string baseUrl)
        {
            this._baseUrl = baseUrl;
        }

        public string GetUrlShortenerApiBaseUrl()
        {
            return this._baseUrl;
        }
    }
}
