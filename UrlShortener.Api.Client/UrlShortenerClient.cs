﻿using Feratel.Deskline.Infrastructure.HttpClient;
using Feratel.Deskline.Infrastructure.HttpClient.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using UrlShortener.Api.Client.Interfaces;
using UrlShortener.Api.Client.Services;

namespace UrlShortener.Api.Client
{
    public static class UrlShortenerClient
    {
        public static IServiceCollection AddUrlShortener(this IServiceCollection services, IUrlShortenerConfiguration config)
        {
            //check if IHttpClient is added
            if (!services.Any(s => s.ServiceType == typeof(IHttpClient)))
            {
                services.AddDesklineHttpClient();
            }

            services.AddSingleton<IUrlShortenerConfiguration>((s) => config);
            services.AddSingleton<IUrlShortenerService, UrlShortenerService>();

            return services;
        }
    }
}
